/**
 * 
 */
package modelo;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import modelo.Tablero;
import modelo.Pieza;
import modelo.Coordenada;
import modelo.Color;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;
/**
 * @author gonzalo
 *
 */
public class TableroTestP4 {

	Tablero t1,t2,t3,t4,t5;
	private PartidaAjedrez paj;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		 t1 = new Tablero(0,2);
		 t2 = new Tablero(2,0);
		 t3 = new Tablero(7,4);
		 t4 = new Tablero(6,1);
		 t5 = new Tablero(27,8);
		
		 //****** P4******
		 paj = PartidaAjedrez.getInstancia();
	}

	@After
	public void tearDown() {
		// P4
		Iterator<Pieza> itb = paj.getPiezas(Color.BLANCO).iterator();
		Iterator<Pieza> itn = paj.getPiezas(Color.NEGRO).iterator();
		
		while(itb.hasNext())
			itb.next().quitaDeCasilla();
		while(itn.hasNext())
			itn.next().quitaDeCasilla();		

	}
	

	//********* P4 **************
	/**
	 * Pb H2
	 * Pn G3
	 * 
	 * Pb amenaza G3
	 * Pn amenaza H2
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testGetAmenazas() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
		PartidaAjedrez paj = PartidaAjedrez.getInstancia();
		Tablero tab =  paj.getTablero();
		
		paj.colocaPieza("Pb H2");
		paj.colocaPieza("Pn G3");
		Coordenada c1 = new Coordenada('H',2);
		Coordenada c2 = new Coordenada('G',3);
		Casilla cas1 = tab.getCasillaAt(c1);
		Pieza p1 = cas1.getPieza();
		Casilla cas2 = tab.getCasillaAt(c2);
		Pieza p2 = cas2.getPieza();

		List<Pieza> l = tab.getAmenazas(cas1,Color.NEGRO);
		assertTrue(l.contains(p2));
		
		l  = tab.getAmenazas(cas2, Color.BLANCO);
		assertTrue(l.contains(p1));
		
		p1.quitaDeCasilla();
		p2.quitaDeCasilla();
	}

	@Test
	public final void testGetFilaDerecha() throws ExcepcionCoordenadaErronea {
		Coordenada c1 = new Coordenada('H',2);
		Coordenada c2 = new Coordenada('G',3);
		Coordenada c3 = new Coordenada('D',2);
		Casilla cas1 = t5.getCasillaAt(c1);

		List<Casilla> l = t5.getFilaDerecha(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(c2);
		l = t5.getFilaDerecha(cas1);
		assertEquals(1,l.size());
		assertEquals(new Coordenada('H',3),l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(c3);
		l = t5.getFilaDerecha(cas1);
		assertEquals(4,l.size());
		assertEquals(new Coordenada('E',2),l.get(0).getCoordenada());
		assertEquals(new Coordenada('H',2),l.get(l.size()-1).getCoordenada());
	}

	@Test
	public final void testGetFilaIzquierda() throws ExcepcionCoordenadaErronea {
		Coordenada c1 = new Coordenada('A',2);
		Coordenada c2 = new Coordenada('B',3);
		Coordenada c3 = new Coordenada('D',2);
		Casilla cas1 = t5.getCasillaAt(c1);

		List<Casilla> l = t5.getFilaIzquierda(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(c2);
		l = t5.getFilaIzquierda(cas1);
		assertEquals(1,l.size());
		assertEquals(new Coordenada('A',3),l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(c3);
		l = t5.getFilaIzquierda(cas1);
		assertEquals(3,l.size());
		assertEquals(new Coordenada('C',2),l.get(0).getCoordenada());
		assertEquals(new Coordenada('A',2),l.get(l.size()-1).getCoordenada());
	}

	@Test
	public final void testGetColumnaArriba() throws ExcepcionCoordenadaErronea {
		Coordenada c1 = new Coordenada('A',8);
		Coordenada c2 = new Coordenada('B',7);
		Coordenada c3 = new Coordenada('D',4);
		Casilla cas1 = t5.getCasillaAt(c1);

		List<Casilla> l = t5.getColumnaArriba(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(c2);
		l = t5.getColumnaArriba(cas1);
		assertEquals(1,l.size());
		assertEquals(new Coordenada('B',8),l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(c3);
		l = t5.getColumnaArriba(cas1);
		assertEquals(4,l.size());
		assertEquals(new Coordenada('D',5),l.get(0).getCoordenada());
		assertEquals(new Coordenada('D',8),l.get(l.size()-1).getCoordenada());
	}

	@Test
	public final void testGetColumnaAbajo() throws ExcepcionCoordenadaErronea {
		Coordenada c1 = new Coordenada('A',1);
		Coordenada c2 = new Coordenada('B',2);
		Coordenada c3 = new Coordenada('D',4);
		Casilla cas1 = t5.getCasillaAt(c1);

		List<Casilla> l = t5.getColumnaAbajo(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(c2);
		l = t5.getColumnaAbajo(cas1);
		assertEquals(1,l.size());
		assertEquals(new Coordenada('B',1),l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(c3);
		l = t5.getColumnaAbajo(cas1);
		assertEquals(3,l.size());
		assertEquals(new Coordenada('D',3),l.get(0).getCoordenada());
		assertEquals(new Coordenada('D',1),l.get(l.size()-1).getCoordenada());
	}
	
	@Test
	public final void testGetSaltosCaballo() throws ExcepcionCoordenadaErronea {
		Coordenada c1 = new Coordenada('A',1);
		Coordenada c2 = new Coordenada('H',8);
		Coordenada c3 = new Coordenada('A',4);
		Coordenada c4 = new Coordenada('H',4);
		Coordenada c5 = new Coordenada('D',4);

		Casilla cas1 = t5.getCasillaAt(c1);
		List<Casilla> l = t5.getSaltosCaballo(cas1);
		assertEquals(2,l.size());
		assertEquals(new Coordenada('B',3),l.get(0).getCoordenada());
		assertEquals(new Coordenada('C',2),l.get(1).getCoordenada());

		cas1 = t5.getCasillaAt(c2);
		l = t5.getSaltosCaballo(cas1);
		assertEquals(2,l.size());
		assertEquals(new Coordenada('G',6),l.get(0).getCoordenada());
		assertEquals(new Coordenada('F',7),l.get(1).getCoordenada());

		cas1 = t5.getCasillaAt(c3);
		l = t5.getSaltosCaballo(cas1);
		assertEquals(4,l.size());
		assertEquals(new Coordenada('B',6),l.get(0).getCoordenada());
		assertEquals(new Coordenada('B',2),l.get(l.size()-1).getCoordenada());

		cas1 = t5.getCasillaAt(c4);
		l = t5.getSaltosCaballo(cas1);
		assertEquals(4,l.size());
		assertEquals(new Coordenada('G',2),l.get(0).getCoordenada());
		assertEquals(new Coordenada('G',6),l.get(l.size()-1).getCoordenada());

		cas1 = t5.getCasillaAt(c5);
		l = t5.getSaltosCaballo(cas1);
		assertEquals(8,l.size());
		assertEquals(new Coordenada('E',6),l.get(0).getCoordenada());
		assertEquals(new Coordenada('C',6),l.get(l.size()-1).getCoordenada());

	}
	
	@Test
	public final void testGetDiagonalNO() throws ExcepcionCoordenadaErronea {
		Coordenada a7 = new Coordenada('A',7);
		Coordenada b6 = new Coordenada('B',6);
		Coordenada c5 = new Coordenada('C',5);
		Coordenada d4 = new Coordenada('D',4);
		Casilla cas1 = t5.getCasillaAt(a7);

		List<Casilla> l = t5.getDiagonalNO(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(b6);
		l = t5.getDiagonalNO(cas1);
		assertEquals(1,l.size());
		assertEquals(a7,l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(d4);
		l = t5.getDiagonalNO(cas1);
		assertEquals(3,l.size());
		assertEquals(c5,l.get(0).getCoordenada());
		assertEquals(a7,l.get(l.size()-1).getCoordenada());
	}

	@Test
	public final void testGetDiagonalNE() throws ExcepcionCoordenadaErronea {
		Coordenada c5 = new Coordenada('C',5);
		Coordenada d6 = new Coordenada('D',6);
		Coordenada e7 = new Coordenada('E',7);
		Coordenada f8 = new Coordenada('F',8);
		Casilla cas1 = t5.getCasillaAt(f8);

		List<Casilla> l = t5.getDiagonalNE(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(e7);
		l = t5.getDiagonalNE(cas1);
		assertEquals(1,l.size());
		assertEquals(f8,l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(c5);
		l = t5.getDiagonalNE(cas1);
		assertEquals(3,l.size());
		assertEquals(d6,l.get(0).getCoordenada());
		assertEquals(f8,l.get(l.size()-1).getCoordenada());
	}

	@Test
	public final void testGetDiagonalSO() throws ExcepcionCoordenadaErronea {
		Coordenada a1 = new Coordenada('A',1);
		Coordenada b2 = new Coordenada('B',2);
		Coordenada c3 = new Coordenada('C',3);
		Coordenada d4 = new Coordenada('D',4);
		
		Casilla cas1 = t5.getCasillaAt(a1);
		List<Casilla> l = t5.getDiagonalSO(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(b2);
		l = t5.getDiagonalSO(cas1);
		assertEquals(1,l.size());
		assertEquals(a1,l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(d4);
		l = t5.getDiagonalSO(cas1);
		assertEquals(3,l.size());
		assertEquals(c3,l.get(0).getCoordenada());
		assertEquals(a1,l.get(l.size()-1).getCoordenada());
	}

	@Test
	public final void testGetDiagonalSE() throws ExcepcionCoordenadaErronea {
		Coordenada g1 = new Coordenada('G',1);
		Coordenada f2 = new Coordenada('F',2);
		Coordenada e3 = new Coordenada('E',3);
		Coordenada d4 = new Coordenada('D',4);
		
		Casilla cas1 = t5.getCasillaAt(g1);
		List<Casilla> l = t5.getDiagonalSE(cas1);
		assertEquals(0,l.size());

		cas1 = t5.getCasillaAt(f2);
		l = t5.getDiagonalSE(cas1);
		assertEquals(1,l.size());
		assertEquals(g1,l.get(0).getCoordenada());
		
		cas1 = t5.getCasillaAt(d4);
		l = t5.getDiagonalSE(cas1);
		assertEquals(3,l.size());
		assertEquals(e3,l.get(0).getCoordenada());
		assertEquals(g1,l.get(l.size()-1).getCoordenada());
	}

	// TODO getFila, Coluna,Diagonal con casillas ocupadas
	
}
