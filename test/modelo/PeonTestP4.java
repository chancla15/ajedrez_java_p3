package modelo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimientoIlegal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PeonTestP4 {

	Pieza p1, pn, pn2, pn3, pb, pb2, pb3;
	Casilla c0,a2,a3,a4,a5,b3,b4,b5,b6,b7,b8,c2,c4,h2,g1,g3;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situación del juego (puedeMover() consulta el tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	@Before
	public void setUp() throws Exception {
		
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		p1 = new Peon(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		pn2 = new Peon(Color.NEGRO);
		pn3 = new Peon(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		pb2 = new Peon(Color.BLANCO);
		pb3 = new Peon(Color.BLANCO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		b8 = tablero.getCasillaAt('B',8);
		c4 = tablero.getCasillaAt('C',4);
		c2 = tablero.getCasillaAt('C',2);
		h2 = tablero.getCasillaAt('H',2);
		g3 = tablero.getCasillaAt('G',3);
		g1 = tablero.getCasillaAt('G',1);
		
	}

	@After
	public void tearDown() {
		p1.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		pb2.quitaDeCasilla();
		pn2.quitaDeCasilla();
		pb3.quitaDeCasilla();
		pn3.quitaDeCasilla();
	}
	

	//********************
	//******* P4 *********
	//********************
	@Test
	public final void testGetCasillasAmenazadasNegro1() {
		pn.setCasilla(b3);
		Set<Casilla> l = pn.getCasillasAmenazadas();

		assertTrue(l.contains(c2));
		assertTrue(l.contains(a2));		
	}

	@Test
	public final void testGetCasillasAmenazadasNegro2() {
		pn.setCasilla(b3);
		pn2.setCasilla(c2);
		pn3.setCasilla(a2);
		Set<Casilla> l = pn.getCasillasAmenazadas();

		assertTrue(l.contains(c2));
		assertTrue(l.contains(a2));		
	}

	@Test
	public final void testGetCasillasAmenazadasNegro3() {
		pn.setCasilla(b3);
		pb2.setCasilla(c2);
		pb3.setCasilla(a2);
		Set<Casilla> l = pn.getCasillasAmenazadas();

		assertTrue(l.contains(c2));
		assertTrue(l.contains(a2));		
	}

	@Test
	public final void testGetCasillasAmenazadasNegro4() {
		// Peon en a4
		pn.setCasilla(a4);
		Set<Casilla> l = pn.getCasillasAmenazadas();

		assertTrue(l.contains(b3));
		assertEquals(1,l.size());		
	}

	@Test
	public final void testGetCasillasAmenazadasNegro5() {
		// Peon en h2
		pn.setCasilla(h2);
		Set<Casilla> l = pn.getCasillasAmenazadas();

		assertTrue(l.contains(g1));
		assertEquals(1,l.size());		
	}

	@Test
	public final void testGetCasillasAmenazadasBlanco1() {
		pb.setCasilla(b3);
		Set<Casilla> l = pb.getCasillasAmenazadas();

		assertTrue(l.contains(c4));
		assertTrue(l.contains(a4));		
	}

	@Test
	public final void testGetCasillasAmenazadasBlanco2() {
		pb.setCasilla(b3);
		pb2.setCasilla(c4);
		pb3.setCasilla(a4);
		Set<Casilla> l = pb.getCasillasAmenazadas();

		assertTrue(l.contains(c4));
		assertTrue(l.contains(a4));		
	}

	@Test
	public final void testGetCasillasAmenazadasBlanco3() {
		pb.setCasilla(b3);
		pn2.setCasilla(c4);
		pn3.setCasilla(a4);
		Set<Casilla> l = pb.getCasillasAmenazadas();

		assertTrue(l.contains(c4));
		assertTrue(l.contains(a4));		
	}

	@Test
	public final void testGetCasillasAmenazadasBlanco4() {
		// Peon en a2
		pb.setCasilla(a2);
		Set<Casilla> l = pb.getCasillasAmenazadas();

		assertTrue(l.contains(b3));
		assertEquals(1,l.size());		
	}

	@Test
	public final void testGetCasillasAmenazadasBlanco5() {
		// Peon en h2
		pb.setCasilla(h2);
		Set<Casilla> l = pb.getCasillasAmenazadas();

		assertTrue(l.contains(g3));
		assertEquals(1,l.size());		
	}

	@Test 
	public final void testisEnUltimaFila() {
		pb.setCasilla(h2);
		assertFalse(((Peon)pb).isEnUltimaFila());
		pb.quitaDeCasilla();
		pb.setCasilla(b8);
		assertTrue(((Peon)pb).isEnUltimaFila());

		pn.setCasilla(h2);
		assertFalse(((Peon)pn).isEnUltimaFila());
		pn.quitaDeCasilla();
		pn.setCasilla(g1);
		assertTrue(((Peon)pn).isEnUltimaFila());
		
	}
}
