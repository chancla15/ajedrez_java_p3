/**
 * 
 */
package modelo;

import static org.junit.Assert.*;

import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimiento;
import modelo.excepciones.ExcepcionMovimientoIlegal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author pierre
 *
 */
public class AlfilTest {

	Pieza al0, an, pn, ab, pb, pb2, pb3;
	Casilla c0,a2,a3,a4,a5,b1,b2,b3,b4,b5,b6,b7,c1,c2,c3,c4,c6,c7,d1,d2,d5;
	Casilla e1,e2,e4,e6,a1,h1,h2,e8,a8,h3,h8,g1,g2,g3,g5,g6,g7,f1,f2,f4,f7;
	Pieza tb,tn,tb2;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situación del juego (puedeMover() consulta el tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		al0 = new Alfil(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		ab = new Alfil(Color.BLANCO);
		an = new Alfil(Color.NEGRO);
		//P4
		tb = new Torre(Color.BLANCO);
		tb2 = new Torre(Color.BLANCO);
		tn = new Torre(Color.NEGRO);
		pb2 = new Peon(Color.BLANCO);
		pb3 = new Peon(Color.BLANCO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		a5 = tablero.getCasillaAt('A',5);
		b1 = tablero.getCasillaAt('B',1);
		b2 = tablero.getCasillaAt('B',2);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		c2 = tablero.getCasillaAt('C',2);
		c3 = tablero.getCasillaAt('C',3);
		c4 = tablero.getCasillaAt('C',4);
		// P4
		a1 = tablero.getCasillaAt('A',1);
		e1 = tablero.getCasillaAt('E',1);
		e2 = tablero.getCasillaAt('E',2);
		e4 = tablero.getCasillaAt('E',4);
		h1 = tablero.getCasillaAt('H',1);
		h2 = tablero.getCasillaAt('H',2);
		h3 = tablero.getCasillaAt('H',3);
		a8 = tablero.getCasillaAt('A',8);
		e8 = tablero.getCasillaAt('E',8);
		h8 = tablero.getCasillaAt('H',8);
		c1 = tablero.getCasillaAt('C',1);
		c6 = tablero.getCasillaAt('C',6);
		c7 = tablero.getCasillaAt('C',7);
		d1 = tablero.getCasillaAt('D',1);
		d2 = tablero.getCasillaAt('D',2);
		d5 = tablero.getCasillaAt('D',5);
		e6 = tablero.getCasillaAt('E',6);
		f1 = tablero.getCasillaAt('F',1);
		f2 = tablero.getCasillaAt('F',2);
		f4 = tablero.getCasillaAt('F',4);
		f7 = tablero.getCasillaAt('F',7);
		g1 = tablero.getCasillaAt('G',1);
		g2 = tablero.getCasillaAt('G',2);
		g3 = tablero.getCasillaAt('G',3);
		g5 = tablero.getCasillaAt('G',5);
		g6 = tablero.getCasillaAt('G',6);
		g7 = tablero.getCasillaAt('G',7);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		al0.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		ab.quitaDeCasilla();
		an.quitaDeCasilla();
		tb.quitaDeCasilla();
		tn.quitaDeCasilla();
		pb2.quitaDeCasilla();
		pb3.quitaDeCasilla();
		tb2.quitaDeCasilla();
	}

	/**
	 * Test method for {@link modelo.Caballo#isMismoTipo(modelo.Pieza)}.
	 */
	@Test
	public final void testIsMismoTipo() {
		assertTrue(ab.isMismoTipo(an));
		assertFalse(ab.isMismoTipo(pb));
	}

	/**
	 * Test method for {@link modelo.Caballo#getTipo()}.
	 */
	@Test
	public final void testGetTipo() {
		assertEquals('A',ab.getTipo());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas() {
		ab.setCasilla(a1);
		Set<Casilla> set = ab.getCasillasAmenazadas();
		assertTrue(set.contains(b2));
		assertTrue(set.contains(c3));
		assertTrue(set.contains(h8));
		assertFalse(set.contains(a1));
		assertEquals(7,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas2() {
		ab.setCasilla(a8);
		Set<Casilla> set = ab.getCasillasAmenazadas();
		assertTrue(set.contains(b7));
		assertTrue(set.contains(h1));
		assertEquals(7,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas3() {
		ab.setCasilla(h8);
		Set<Casilla> set = ab.getCasillasAmenazadas();
		assertTrue(set.contains(g7));
		assertTrue(set.contains(a1));
		assertEquals(7,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas4() {
		ab.setCasilla(h1);
		Set<Casilla> set = ab.getCasillasAmenazadas();
		assertTrue(set.contains(g2));
		assertTrue(set.contains(a8));
		assertEquals(7,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas5() {
		ab.setCasilla(d5);
		Set<Casilla> set = ab.getCasillasAmenazadas();
		assertTrue(set.contains(c4));
		assertTrue(set.contains(a2));
		assertTrue(set.contains(e6));
		assertTrue(set.contains(f7));
		assertTrue(set.contains(c6));
		assertTrue(set.contains(a8));
		assertTrue(set.contains(e4));
		assertTrue(set.contains(h1));

		assertEquals(13,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 * 
	 * Comprueba casilla amenazadas con piezas interpuestas
	 */
	@Test
	public final void testGetCasillasAmenazadas6() {
		ab.setCasilla(d5);
		an.setCasilla(b7);
		pb.setCasilla(a2);
		pn.setCasilla(e6);
		pb2.setCasilla(g2);
		Set<Casilla> set = ab.getCasillasAmenazadas();
		assertTrue(set.contains(c4));
		assertTrue(set.contains(a2));
		assertTrue(set.contains(e6));
		assertFalse(set.contains(f7));
		assertTrue(set.contains(c6));
		assertFalse(set.contains(a8));
		assertTrue(set.contains(e4));
		assertFalse(set.contains(h1));

		assertEquals(9,set.size());
	}

	/**
	 * Test method for {@link modelo.Pieza#puedeMover(modelo.Casilla)}.
	 * Puede mover sin piezas interpuestas
	 */
	@Test
	public final void testPuedeMover() {
		ab.setCasilla(c3);
		assertTrue(ab.puedeMover(a1));
		assertTrue(ab.puedeMover(a5));
		assertTrue(ab.puedeMover(e1));
		assertTrue(ab.puedeMover(h8));

		assertFalse(ab.puedeMover(c3));
		assertFalse(ab.puedeMover(c4));
		
	}

	/**
	 * Test method for {@link modelo.Pieza#puedeMover(modelo.Casilla)}.
	 * Puede mover a casilla ocupada por el contrario, pero no por pieza del mismo color
	 */
	@Test
	public final void testPuedeMover2() {
		ab.setCasilla(c3);
		pb.setCasilla(a5);
		pn.setCasilla(e1);
		assertFalse(ab.puedeMover(a5));
		assertTrue(ab.puedeMover(e1));		
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 * 
	 * Comprueba haMovido() para el alfil
	 */
	@Test
	public final void testMueve0() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		assertFalse(ab.haMovido());
		ab.setCasilla(c3);
		try {
			ab.mueve(c2);
		} catch (ExcepcionMovimiento ex) {
			// nada
		}
		assertFalse(ab.haMovido());
		ab.mueve(b2);
		assertTrue(ab.haMovido());
	}
 
	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada
	 * 
	 *  Comprueba que el afil mueve
	 */
	@Test
	public final void testMueve() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		ab.setCasilla(c3);
		ab.mueve(a1);
		assertSame(a1,ab.getCasilla());
		assertNull(c3.getPieza());
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 * 
	 * Comprueba movimiento ilegal
	 */
	@Test(expected=ExcepcionMovimientoIlegal.class)
	public final void testMueve2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		ab.setCasilla(c3);
		ab.mueve(f7);
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 */
	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testMueve3() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		ab.setCasilla(c3);
		pb.setCasilla(a1);
		ab.mueve(a1);
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada
	 * 
	 *  Comprueba que el afil captura
	 */
	@Test
	public final void testCaptura() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		ab.setCasilla(d5);
		pn.setCasilla(f7);
		ab.mueve(f7);
		assertSame(f7,ab.getCasilla());
		assertNull(d5.getPieza());
		assertNull(pn.getCasilla());
	}

}
