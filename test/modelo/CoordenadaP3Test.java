package modelo;

import static org.junit.Assert.*;

import modelo.excepciones.ExcepcionCoordenadaErronea;

import org.junit.Before;
import org.junit.Test;

/**
@author drizo
@date 22/08/2011
 **/
public class CoordenadaP3Test {
	Coordenada c;
	
	@Before
	public void setUp() throws Exception {
		c = new Coordenada('F', 5);		
	}

	@Test
	public final void testGetters() {
		assertEquals("Letra", 'F', c.getLetra());
		assertEquals("y", 5, c.getY());
	}

	@Test
	public final void testInicializacion() {
		Coordenada c3 = new Coordenada();
		assertEquals("c3.x", '0', c3.getLetra());
		assertEquals("c3.y", 0, c3.getY());
		Coordenada c2 = new Coordenada(c);
		assertEquals("c2.letra", c2.getLetra(), c.getLetra());
		assertEquals("c2.y", c2.getY(), c.getY());
	}

	/**
	 * @throws ExcepcionCoordenadaErronea
	 */
	@Test(expected=ExcepcionCoordenadaErronea.class)
	public void testInicializacion2() throws ExcepcionCoordenadaErronea {
		Coordenada c3;
		c3 = new Coordenada('*',100);
	}
	
	@Test
	public final void testEquals() throws ExcepcionCoordenadaErronea {
		Coordenada c4 = new Coordenada('F',5);
		Coordenada c5 = new Coordenada('F',6);
		String s = new String();
		assertFalse(c.equals(null));
		assertFalse(c.equals(s));
		assertFalse(c.equals(c5));
		assertTrue(c.equals(c));
		assertTrue(c.equals(c4));
	
	}
	
	@Test
	public final void testCoordenadaError() {
		assertEquals("coordenadaError.getLetra()",Coordenada.coordenadaError.getLetra(),'0');
		assertEquals("coordenadaError.getY()",Coordenada.coordenadaError.getY(),0);
	}
	
	@Test
	public final void testToString() {
		assertEquals("Coordenada.toString()",c.toString(),"F5");
		assertEquals("Coordenada.toString()",Coordenada.coordenadaError.toString(),"00");
	}
	
	
	
}
