package modelo;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import modelo.excepciones.ExcepcionCoordenadaErronea;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MovimientoTestP3 {


	@Test
	public final void testGetCoordenadaOrigen() throws ExcepcionCoordenadaErronea {
		Coordenada co = new Coordenada('A', 2);
		Coordenada cd = new Coordenada('A', 3);
		Movimiento m = new MovimientoOrdinario(co, cd);
		
		assertEquals(co,m.getCoordenadaOrigen());
		assertEquals(cd,m.getCoordenadaDestino());		
		// comprobamos la implementacion de la composicion
		assertNotSame(cd,m.getCoordenadaDestino());
		assertNotSame(co,m.getCoordenadaOrigen());
	}
	


}
