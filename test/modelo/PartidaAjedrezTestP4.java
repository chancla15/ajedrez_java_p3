package modelo;
import static org.junit.Assert.*;

import java.util.Iterator;

import modelo.Casilla;
import modelo.Color;
import modelo.Coordenada;
import modelo.PartidaAjedrez;
import modelo.Pieza;
import modelo.Tablero;
import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoCoronacion;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class PartidaAjedrezTestP4 {

	static PartidaAjedrez paj;
	
	Peon pb,pb2; 
	Pieza db;
	Peon pn,pn2; 
	Pieza dn;
	Pieza rn;

	@BeforeClass
	public static void setUpClass() throws Exception {
		paj=PartidaAjedrez.getInstancia();
	}
	
	@Before
	public void setUp() throws Exception {
		 pb = new Peon(Color.BLANCO);
		 db = new Dama(Color.BLANCO);
		 pn = new Peon(Color.NEGRO);
		 dn = new Dama(Color.NEGRO);
		 pb2 = new Peon(Color.BLANCO);
		 pn2 = new Peon(Color.NEGRO);
		 rn = new Rey(Color.NEGRO);

	}

	@After
	public void tearDown() throws Exception {
		 pb.quitaDeCasilla();
		 db.quitaDeCasilla();
		 pn.quitaDeCasilla();
		 dn.quitaDeCasilla();
		 pb2.quitaDeCasilla();
		 pn2.quitaDeCasilla();
		 rn.quitaDeCasilla();
	// P4	 
			Iterator<Pieza> itb = paj.getPiezas(Color.BLANCO).iterator();
			Iterator<Pieza> itn = paj.getPiezas(Color.NEGRO).iterator();
			
			while(itb.hasNext())
				itb.next().quitaDeCasilla();
			while(itn.hasNext())
				itn.next().quitaDeCasilla();		

	}


// ********* P4 *************

	@Test
	public final void testCoronarBlanco() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		pb.setCasilla(paj.getTablero().getCasillaAt('A',8));
		paj.coronar(pb, db);
	}

	@Test
	public final void testCoronarNegro() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		pn.setCasilla(paj.getTablero().getCasillaAt('A',1));
		paj.coronar(pn, dn);
	}

	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void testExcepcionCoronacion1() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		// peon blanco que no esta en ultima fila
		pb.setCasilla(paj.getTablero().getCasillaAt('A',7));
		paj.coronar(pb, db);
	}

	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void testExcepcionCoronacion2() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		// peon negro que no esta en ultima fila
		pn.setCasilla(paj.getTablero().getCasillaAt('A',2));
		paj.coronar(pn, db);
	}

	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void testExcepcionCoronacion3() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		// Coronar a rey
		pn.setCasilla(paj.getTablero().getCasillaAt('A',1));
		paj.coronar(pn, rn);
	}

	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void testExcepcionCoronacion4() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		// Coronar a peon
		pn.setCasilla(paj.getTablero().getCasillaAt('A',1));
		paj.coronar(pn, pn2);
	}

	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void testExcepcionCoronacion5() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		// Coronar a pieza que ya esta en el tablero
		pn.setCasilla(paj.getTablero().getCasillaAt('A',1));
		dn.setCasilla(paj.getTablero().getCasillaAt('A',4));
		paj.coronar(pn, dn);
	}
	
	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void testExcepcionCoronacion6() throws ExcepcionCoordenadaErronea, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionMovimientoCoronacion {
		// Coronar a pieza de distinto color
		pn.setCasilla(paj.getTablero().getCasillaAt('A',1));
		paj.coronar(pn, db);
	}

}
