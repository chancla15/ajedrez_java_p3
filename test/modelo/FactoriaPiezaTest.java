package modelo;

import static org.junit.Assert.*;

import modelo.excepciones.ExcepcionPiezaDesconocida;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FactoriaPiezaTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testCreaPiezaStringColor() throws ExcepcionPiezaDesconocida {
		Pieza p = FactoriaPieza.creaPieza("Peon", Color.BLANCO);
		assertTrue(p instanceof Peon);
		assertEquals(Color.BLANCO,p.getColor());

		p = FactoriaPieza.creaPieza("Peon", Color.NEGRO);
		assertTrue(p instanceof Peon);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza("Rey", Color.NEGRO);
		assertTrue(p instanceof Rey);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza("Torre", Color.NEGRO);
		assertTrue(p instanceof Torre);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza("Alfil", Color.NEGRO);
		assertTrue(p instanceof Alfil);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza("Caballo", Color.NEGRO);
		assertTrue(p instanceof Caballo);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza("Dama", Color.NEGRO);
		assertTrue(p instanceof Dama);
		assertEquals(Color.NEGRO,p.getColor());
		
	}

	@Test(expected=ExcepcionPiezaDesconocida.class)
	public final void testCreaPiezaRara() throws ExcepcionPiezaDesconocida {
		Pieza p = FactoriaPieza.creaPieza("Rara", Color.BLANCO);
	}
	
	@Test
	public final void testCreaPiezaCharColor() throws ExcepcionPiezaDesconocida {
		Pieza p = FactoriaPieza.creaPieza('P', Color.BLANCO);
		assertTrue(p instanceof Peon);
		assertEquals(Color.BLANCO,p.getColor());

		p = FactoriaPieza.creaPieza('P', Color.NEGRO);
		assertTrue(p instanceof Peon);
		assertEquals(Color.NEGRO,p.getColor());


		p = FactoriaPieza.creaPieza('R', Color.NEGRO);
		assertTrue(p instanceof Rey);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza('T', Color.NEGRO);
		assertTrue(p instanceof Torre);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza('A', Color.NEGRO);
		assertTrue(p instanceof Alfil);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza('C', Color.NEGRO);
		assertTrue(p instanceof Caballo);
		assertEquals(Color.NEGRO,p.getColor());

		p = FactoriaPieza.creaPieza('D', Color.NEGRO);
		assertTrue(p instanceof Dama);
		assertEquals(Color.NEGRO,p.getColor());
	}

}
