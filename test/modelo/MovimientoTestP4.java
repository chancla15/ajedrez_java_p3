package modelo;

import static org.junit.Assert.*;

import java.lang.reflect.Modifier;

import modelo.excepciones.ExcepcionCoordenadaErronea;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MovimientoTestP4 {

	// ***** P4 *****
	
	/**
	 * Comprueba si Movimiento es abstracta
	 */
	@Test
	public final void testMovimientoAbstracto() {
		Class<Movimiento> cmov = Movimiento.class;
		assertTrue( Modifier.isAbstract( cmov.getModifiers()) );
	}
	
	/**
	 * Test method for {@link modelo.MovimientoCoronacion#MovimientoCoronacion(modelo.Coordenada, modelo.Coordenada, modelo.Pieza)}.
	 * @throws ExcepcionCoordenadaErronea 
	 */
	@Test
	public final void testMovimientoCoronacion() throws ExcepcionCoordenadaErronea {
		Coordenada c2,c1;
		Pieza p;
		c1 = new Coordenada('C', 1);
		c2 = new Coordenada('C', 2);
		p = new Dama(Color.BLANCO);
		MovimientoCoronacion m = new MovimientoCoronacion(c2, c1, p);
		assertEquals(c2,m.getCoordenadaOrigen());
		assertEquals(c1,m.getCoordenadaDestino());
		assertSame(p,m.getPieza());
	}

}
