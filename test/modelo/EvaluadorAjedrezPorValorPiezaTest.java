/**
 * 
 */
package modelo;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.Iterator;

import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimiento;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author pierre
 *
 */
public class EvaluadorAjedrezPorValorPiezaTest {

	  PartidaAjedrez paj;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public final void tearDown() throws Exception {
		Iterator<Pieza> itp = paj.getPiezas(Color.BLANCO).iterator();
		while (itp.hasNext())
			itp.next().quitaDeCasilla();
		itp = paj.getPiezas(Color.NEGRO).iterator();
		while (itp.hasNext())
			itp.next().quitaDeCasilla();
	}

	/**
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evalua(modelo.PartidaAjedrez, modelo.Color)}.
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testEvalua() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		paj = PartidaAjedrez.getInstancia();
		paj.colocaPieza("Pb E2");
		paj.colocaPieza("Pn E7");
		paj.colocaPieza("Rb E1");
		paj.colocaPieza("Rn E8");
		paj.colocaPieza("Db D1");
		paj.colocaPieza("Dn D8");
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorValorPiezas();
		assertEquals(10.0, eva.evalua(paj, Color.BLANCO), 0.001);
		assertEquals(10.0, eva.evalua(paj, Color.NEGRO), 0.001);
	}

	/**
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evalua(modelo.PartidaAjedrez, modelo.Color)}.
	 * @throws FileNotFoundException 
	 */
	@Test
	public final void testEvaluaPosicionInicial() throws FileNotFoundException {
		paj = PartidaAjedrez.getInstancia();
		paj.setEntrada("test/sinpos_sinmov.ent");
		paj.setSalida("test/sinpos_sinmov.out");
		paj.run();
		// Deja el tablero con las piezas en posicion inicial.
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorValorPiezas();
		System.out.println(paj.toString());
		System.out.println("BLANCAS EVA ");
		System.out.println(eva.evalua(paj, Color.BLANCO));
		//assertEquals(39.0, eva.evalua(paj, Color.BLANCO), 0.001);
		//assertEquals(39.0, eva.evalua(paj, Color.NEGRO), 0.001);
	}

	/**
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evaluaMovimiento(modelo.PartidaAjedrez, modelo.Color, int)}.
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionMovimiento 
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testEvaluaMovimiento() throws ExcepcionCoordenadaErronea, ExcepcionMovimiento, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
		paj = PartidaAjedrez.getInstancia();
		paj.colocaPieza("Pb E2");
		paj.colocaPieza("Pn E7");
		paj.colocaPieza("Rb E1");
		paj.colocaPieza("Rn E8");
		paj.colocaPieza("Db D1");
		paj.colocaPieza("Dn D8");
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorValorPiezas();
		Coordenada co = new Coordenada('D', 1);
		Coordenada cd = new Coordenada('D', 8);
		paj.addMovimiento(co,cd);
		assertEquals(1.0,eva.evaluaMovimiento(paj, Color.NEGRO, 0), 0.001);
		
	}

}
