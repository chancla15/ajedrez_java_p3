package modelo;

import static org.junit.Assert.*;

import java.util.Iterator;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimiento;
import modelo.excepciones.ExcepcionMovimientoCoronacion;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;
import modelo.excepciones.ExcepcionTurnoDelContrario;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExcepcionesTestSuiteP3 {

	Pieza peon;
	Pieza rey;
	Pieza dama;
	Pieza torre;
	PartidaAjedrez paj;
	
	@Before
	public final void setUpBefore() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		paj.colocaPieza("Pb E8");
		paj.colocaPieza("Pb B7");
		paj.colocaPieza("Tb A8");
		paj.colocaPieza("Pn E2");
		peon = new Peon(Color.BLANCO);
		rey = new Rey(Color.BLANCO);
		dama = new Dama(Color.BLANCO);
		torre = new Torre(Color.NEGRO);
	}
	
	@After
	public final void tearUp() throws ExcepcionCoordenadaErronea {
/*		Pieza p = paj.getPiezaAt(new Coordenada('E',8));
		p.quitaDeCasilla();
		 p = paj.getPiezaAt(new Coordenada('B',7));
			p.quitaDeCasilla();
		 p = paj.getPiezaAt(new Coordenada('A',8));
			p.quitaDeCasilla();
		 p = paj.getPiezaAt(new Coordenada('E',2));
			p.quitaDeCasilla();
			peon.quitaDeCasilla();
			rey.quitaDeCasilla();
			dama.quitaDeCasilla();
			torre.quitaDeCasilla();
	*/		
			Iterator<Pieza> itb = paj.getPiezas(Color.BLANCO).iterator();
			Iterator<Pieza> itn = paj.getPiezas(Color.NEGRO).iterator();
			
			while(itb.hasNext())
				itb.next().quitaDeCasilla();
			while(itn.hasNext())
				itn.next().quitaDeCasilla();		

	}
	
	/**
	 * Esta excepción se lanza cuando se pretende

    coronar una pieza que no es un peón.
    coronar un peón cuya casilla de destino no está en su última fila (fila 8 para peones blancos, fila 1 para peones negros).
    coronar un peón a un peón o a un rey.
    coronar a una pieza que ya está en el tablero.
    coronar a puna pieza de distinto color.

 */
	/**
	 * Excepcion al coronar a pieza de distinto color
	 * @throws ExcepcionCoordenadaErronea
	 * @throws ExcepcionMovimientoCoronacion 
	 */
	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void tesExcepcionMovimientoCoronacion() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
			Pieza pe = paj.getPiezaAt(new Coordenada('E',8));
			paj.coronar((Peon)pe, torre);
	}

	/**
	 * Excepcion al coronar peón que no está en útima fila
	 * @throws ExcepcionCoordenadaErronea
	 * @throws ExcepcionMovimientoCoronacion 
	 */
	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void tesExcepcionMovimientoCoronacion2() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
			Pieza pe = paj.getPiezaAt(new Coordenada('B',7));
			paj.coronar((Peon)pe, dama);
	}

	/**
	 * Excepcion al coronar peón que no está en útima fila
	 * @throws ExcepcionCoordenadaErronea
	 * @throws ExcepcionMovimientoCoronacion 
	 */
	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void tesExcepcionMovimientoCoronacion2Negro() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
			Pieza pe = paj.getPiezaAt(new Coordenada('E',2));
			paj.coronar((Peon)pe, dama);
	}

	/**
	 * Excepcion al coronar peón a peón
	 * @throws ExcepcionCoordenadaErronea
	 * @throws ExcepcionMovimientoCoronacion 
	 */
	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void tesExcepcionMovimientoCoronacion3() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
			Pieza pe = paj.getPiezaAt(new Coordenada('E',8));
			paj.coronar((Peon)pe, peon);
	}

	/**
	 * Excepcion al coronar peón a rey
	 * @throws ExcepcionCoordenadaErronea
	 * @throws ExcepcionMovimientoCoronacion 
	 */
	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void tesExcepcionMovimientoCoronacion4() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
			Pieza pe = paj.getPiezaAt(new Coordenada('E',8));
			paj.coronar((Peon)pe, rey);
	}

	/**
	 * Excepcion al coronar peón a pieza en tablero
	 * @throws ExcepcionCoordenadaErronea
	 * @throws ExcepcionMovimientoCoronacion 
	 */
	@Test(expected=ExcepcionMovimientoCoronacion.class)
	public final void tesExcepcionMovimientoCoronacion5() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
			Pieza pe = paj.getPiezaAt(new Coordenada('E',8));
			Pieza pi = paj.getPiezaAt(new Coordenada('A',8));
			paj.coronar((Peon)pe, pi);
	}


}
