package modelo;

import static org.junit.Assert.*;

import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimientoIlegal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ReyTestP4 {

	Pieza r0, rn, pn, rb, pb, pb2, pb3;
	Casilla c0,a2,a3,a4,a5,b1,b2,b3,b4,b5,b6,b7,c1,c2,c3,c4,c8,d1,d2,d8;
	Casilla e1,a1,h1,h2,e8,a8,h8,g1,g2,g8,f1,f2,f8;
	Pieza tb,tn,tb2;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situación del juego (puedeMover() consulta el tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	@Before
	public void setUp() throws Exception {
		
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		r0 = new Rey(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		rn = new Rey(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		rb = new Rey(Color.BLANCO);
		//P4
		tb = new Torre(Color.BLANCO);
		tb2 = new Torre(Color.BLANCO);
		tn = new Torre(Color.NEGRO);
		pb2 = new Peon(Color.BLANCO);
		pb3 = new Peon(Color.BLANCO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		a5 = tablero.getCasillaAt('A',5);
		b1 = tablero.getCasillaAt('B',1);
		b2 = tablero.getCasillaAt('B',2);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		c2 = tablero.getCasillaAt('C',2);
		c3 = tablero.getCasillaAt('C',3);
		c4 = tablero.getCasillaAt('C',4);
		// P4
		a1 = tablero.getCasillaAt('A',1);
		e1 = tablero.getCasillaAt('E',1);
		h1 = tablero.getCasillaAt('H',1);
		h2 = tablero.getCasillaAt('H',2);
		a8 = tablero.getCasillaAt('A',8);
		e8 = tablero.getCasillaAt('E',8);
		h8 = tablero.getCasillaAt('H',8);
		c1 = tablero.getCasillaAt('C',1);
		d1 = tablero.getCasillaAt('D',1);
		d2 = tablero.getCasillaAt('D',2);
		d8 = tablero.getCasillaAt('D',8);
		f1 = tablero.getCasillaAt('F',1);
		f2 = tablero.getCasillaAt('F',2);
		f8 = tablero.getCasillaAt('F',8);
		g1 = tablero.getCasillaAt('G',1);
		g2 = tablero.getCasillaAt('G',2);
		c8 = tablero.getCasillaAt('C',8);
		g8 = tablero.getCasillaAt('G',8);

	}

	@After
	public void tearDown() {
		r0.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		rb.quitaDeCasilla();
		rn.quitaDeCasilla();
		tb.quitaDeCasilla();
		tn.quitaDeCasilla();
		pb2.quitaDeCasilla();
		pb3.quitaDeCasilla();
		tb2.quitaDeCasilla();
	}
	


	//************** P4 *************
	/**
	 * Rey en casilla interior, sin otras piezas.
	 */
	@Test
	public final void testReyGetCasillasAmenazadas1() {		
		rb.setCasilla(b3);
		//System.out.print(rb.getCasillasAmenazadas());
		Set<Casilla> set = rb.getCasillasAmenazadas();
		assertTrue(set.contains(a3));
		assertTrue(set.contains(c3));
		assertTrue(set.contains(a4));
		assertTrue(set.contains(b4));
		assertTrue(set.contains(c4));
		assertTrue(set.contains(a2));
		assertTrue(set.contains(b2));
		assertTrue(set.contains(c2));
		assertEquals(8,set.size());
	}
	
	/**
	 * Rey en casilla interior, con otras piezas del mismo color.
	 */
	@Test
	public final void testReyGetCasillasAmenazadas2() {		
		rn.setCasilla(b3);
		pn.setCasilla(c2);
		tn.setCasilla(a4);
		
		//System.out.print(rb.getCasillasAmenazadas());
		Set<Casilla> set = rn.getCasillasAmenazadas();
		assertTrue(set.contains(c2));
		assertTrue(set.contains(a4));
		assertEquals(8,set.size());
	}

	/**
	 * Rey en casilla interior, con otras piezas de distinto color.
	 */
	@Test
	public final void testReyGetCasillasAmenazadas3() {		
		rn.setCasilla(b3);
		pb.setCasilla(c2);
		tb.setCasilla(a4);
		
		//System.out.print(rb.getCasillasAmenazadas());
		Set<Casilla> set = rn.getCasillasAmenazadas();
		assertTrue(set.contains(c2));
		assertTrue(set.contains(a4));
		assertEquals(8,set.size());
	}
	
	/**
	 * Rey en casilla lateral, sin otras piezas.
	 */
	@Test
	public final void testReyGetCasillasAmenazadas4() {		
		rn.setCasilla(a3);
		
		//System.out.print(rb.getCasillasAmenazadas());
		Set<Casilla> set = rn.getCasillasAmenazadas();
		assertTrue(set.contains(a4));
		assertTrue(set.contains(b4));
		assertTrue(set.contains(b3));
		assertTrue(set.contains(a2));
		assertTrue(set.contains(b2));
		assertEquals(5,set.size());
	}

	// Hazlo tú: Rey en los otros tres laterales del tablero

	/**
	 * Rey en esquina, sin otras piezas.
	 */
	@Test
	public final void testReyGetCasillasAmenazadas5() {		
		rn.setCasilla(a1);
		
		//System.out.print(rb.getCasillasAmenazadas());
		Set<Casilla> set = rn.getCasillasAmenazadas();
		assertTrue(set.contains(a2));
		assertTrue(set.contains(b2));
		assertTrue(set.contains(b1));
		assertEquals(3,set.size());

	}
	
	// Hazlo tú: Rey en las otras tres esquinas

	// Hazlo tú: Rey en las otras tres esquinas o laterales del tablero, con piezas de cualquier
	// color en las casillas amenazadas.

	/**
	 * comprobamos que se puede enrocar, e1-a1 y e1-h1
	 */
	@Test 
	public final void testComprobarEnroque() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1);
		tb.setCasilla(a1);
		assertTrue(((Rey)rb).comprobarEnroque(a1));

		tb.quitaDeCasilla();
		tb.setCasilla(h1);
		assertTrue(((Rey)rb).comprobarEnroque(h1));
		assertFalse(((Rey)rb).comprobarEnroque(a2));
		
	}

	/**
	 * No se puede enrocar si la torre ha movido (e1-h1)
	 * @throws ExcepcionCasillaDestinoOcupada
	 * @throws ExcepcionMovimientoIlegal
	 */
	@Test 
	public final void testComprobarEnroque2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1);
		tb.setCasilla(h1);
		tb.mueve(h8);
		tb.mueve(h1);
		assertFalse(((Rey)rb).comprobarEnroque(h1));
		
	}

	/**
	 * No se puede enrocar si el rey ha movido
	 * @throws ExcepcionCasillaDestinoOcupada
	 * @throws ExcepcionMovimientoIlegal
	 */
	@Test 
	public final void testComprobarEnroque3() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1);
		tb.setCasilla(a1);
		rb.mueve(d1);
		rb.mueve(e1);
		assertFalse(((Rey)rb).comprobarEnroque(a1));
		
	}

	/**
	 * No se puede enrocar con una torre de otro color
	 * @throws ExcepcionCasillaDestinoOcupada
	 * @throws ExcepcionMovimientoIlegal
	 */
	@Test 
	public final void testComprobarEnroque4() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1); // no ha movido
		tn.setCasilla(a1); // no ha movido
		assertFalse(((Rey)rb).comprobarEnroque(a1));
		
	}

	/**
	 * No se puede enrocar si hay piezas interpuestas (de cualquier color)
	 * @throws ExcepcionCasillaDestinoOcupada
	 * @throws ExcepcionMovimientoIlegal
	 */
	@Test 
	public final void testComprobarEnroque5() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1); // no ha movido
		tb.setCasilla(a1); // no ha movido
		pb.setCasilla(b1); // no ha movido
		assertFalse(((Rey)rb).comprobarEnroque(a1));

		pb.quitaDeCasilla();
		pb2.setCasilla(c1);
		assertFalse(((Rey)rb).comprobarEnroque(a1));

		pb2.quitaDeCasilla();
		pn.setCasilla(d1);
		assertFalse(((Rey)rb).comprobarEnroque(a1));
		
		tb2.setCasilla(h1);
		pb.setCasilla(f1);
		assertFalse(((Rey)rb).comprobarEnroque(h1));

		pb.quitaDeCasilla();
		pb2.setCasilla(g1);
		assertFalse(((Rey)rb).comprobarEnroque(a1));
	
	}

	/**
	 * No se puede enrocar si el rey está amenazado 
	 * @throws ExcepcionCasillaDestinoOcupada
	 * @throws ExcepcionMovimientoIlegal
	 */
	@Test 
	public final void testComprobarEnroque6() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1); // no ha movido
		tb.setCasilla(a1); // no ha movido
		pn.setCasilla(f2); // no ha movido
		assertFalse(((Rey)rb).comprobarEnroque(a1));

		tb.quitaDeCasilla();
		tb.setCasilla(h1); // no ha movido
		assertFalse(((Rey)rb).comprobarEnroque(h1));
		
	}

	/**
	 * Se puede enrocar si 
	 * largo: la torre a1 está amenazada o b1 está amemazada 
	 * corto: la torre h1 está amenazada
	 * @throws ExcepcionCasillaDestinoOcupada
	 * @throws ExcepcionMovimientoIlegal
	 */
	@Test 
	public final void testComprobarEnroque7() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1); // no ha movido
		tb.setCasilla(a1); // no ha movido
		tn.setCasilla(a2); // no ha movido
		assertTrue(((Rey)rb).comprobarEnroque(a1));
		
		tn.quitaDeCasilla();
		tn.setCasilla(b2);
		assertTrue(((Rey)rb).comprobarEnroque(a1));
		
		tb.quitaDeCasilla();
		tb.setCasilla(h1);
		tn.quitaDeCasilla();
		tn.setCasilla(h2); // no ha movido
		assertTrue(((Rey)rb).comprobarEnroque(h1));		
	}

	/**
	 * No se puede enrocar si 
	 * corto: f1 o g1 están amenazadas 
	 * largo: c1 o d1 están amenazadas
	 * @throws ExcepcionCasillaDestinoOcupada
	 * @throws ExcepcionMovimientoIlegal
	 */
	@Test 
	public final void testComprobarEnroque8() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(e1); // no ha movido
		tb.setCasilla(a1); // no ha movido
		tn.setCasilla(c2); // no ha movido
		assertFalse(((Rey)rb).comprobarEnroque(a1));
		
		tn.quitaDeCasilla();
		tn.setCasilla(d2);
		assertFalse(((Rey)rb).comprobarEnroque(a1));
		
		tb.quitaDeCasilla();
		tb.setCasilla(h1);
		tn.quitaDeCasilla();
		tn.setCasilla(f2);
		assertFalse(((Rey)rb).comprobarEnroque(h1));	
		
		tn.quitaDeCasilla();
		tn.setCasilla(g2);
		assertFalse(((Rey)rb).comprobarEnroque(h1));
		
	}

	/**
	 * comprobamos que se puede enrocar, e8-a8 y e8-h8
	 */
	@Test 
	public final void testComprobarEnroqueNegro() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rn.setCasilla(e8);
		tn.setCasilla(a8);
		assertTrue(((Rey)rn).comprobarEnroque(a8));

		tn.quitaDeCasilla();
		tn.setCasilla(h8);
		assertTrue(((Rey)rn).comprobarEnroque(h8));
		assertFalse(((Rey)rn).comprobarEnroque(h2));
		
	}

	// Hazlo tú: Crea tests para comprobar el enroque del rey negro en diferentes circunstancias.


}
