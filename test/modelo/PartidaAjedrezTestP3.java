package modelo;
import static org.junit.Assert.*;

import java.util.Iterator;

import modelo.Casilla;
import modelo.Color;
import modelo.Coordenada;
import modelo.PartidaAjedrez;
import modelo.Pieza;
import modelo.Tablero;
import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoCoronacion;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class PartidaAjedrezTestP3 {

	static PartidaAjedrez paj;
	
	Peon pb,pb2; 
	Pieza db;
	Peon pn,pn2; 
	Pieza dn;
	Pieza rn;

	@BeforeClass
	public static void setUpClass() throws Exception {
		paj=PartidaAjedrez.getInstancia();
	}
	
	@Before
	public void setUp() throws Exception {
		 pb = new Peon(Color.BLANCO);
		 db = new Dama(Color.BLANCO);
		 pn = new Peon(Color.NEGRO);
		 dn = new Dama(Color.NEGRO);
		 pb2 = new Peon(Color.BLANCO);
		 pn2 = new Peon(Color.NEGRO);
		 rn = new Rey(Color.NEGRO);

	}

	@After
	public void tearDown() throws Exception {
		 pb.quitaDeCasilla();
		 db.quitaDeCasilla();
		 pn.quitaDeCasilla();
		 dn.quitaDeCasilla();
		 pb2.quitaDeCasilla();
		 pn2.quitaDeCasilla();
		 rn.quitaDeCasilla();
	// P4	 
			Iterator<Pieza> itb = paj.getPiezas(Color.BLANCO).iterator();
			Iterator<Pieza> itn = paj.getPiezas(Color.NEGRO).iterator();
			
			while(itb.hasNext())
				itb.next().quitaDeCasilla();
			while(itn.hasNext())
				itn.next().quitaDeCasilla();		

	}

	@Test
	public final void testPartidaAjedrez() throws ExcepcionCoordenadaErronea {
		// Comprobamos el tablero 8x8

		Tablero t = paj.getTablero();
		Coordenada coor;
			//Prueba Diagonal tablero Negras para getCasilla(Coordenada)
			for (int i=0; i<8; i++) {
				coor = new Coordenada( (char)((int)'A'+i),i+1 );
				assertEquals( "Diagonal negras",Color.NEGRO,(t.getCasillaAt(coor)).getColor() );
			}
			
			//Prueba Diagonal tablero Blancas para getCasilla(Coordenada)
			int j=7;
			for (int i=0; i<8; i++) {
				coor = new Coordenada((char)((int)'A'+j),i+1 );
				assertEquals( "Diagonal blancas",Color.BLANCO,(t.getCasillaAt(coor)).getColor() );
				j--;
			}
			
			// Tablero vacio
			Pieza p=null;
			for (int i=0; i<8; i++) {
				for (j=7; j>=0; j--) {
					coor = new Coordenada((char)((int)'A'+j),i+1 );
					p=t.getCasillaAt(coor).getPieza();
					assertNull(p);
				}
			}
			
			// Sin movimientos
			// assertEquals(0, paj.getNumMovimientos());
	}

	@Test
	public final void testGetInstancia() {
		PartidaAjedrez p = PartidaAjedrez.getInstancia();
		p = PartidaAjedrez.getInstancia();
		assertSame(paj,p);
	}

	@Test
	public final void testGetPiezaAt() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		pb.setCasilla(paj.getTablero().getCasillaAt('A',2));
		Pieza p = paj.getPiezaAt(new Coordenada('A', 2));
		assertSame(pb,p);
		pb.quitaDeCasilla();
	}

	@Test
	public final void testColocaPieza() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida  {
		paj.colocaPieza("Pb F3");
		Pieza p = paj.getPiezaAt(new Coordenada('F', 3));
		p.quitaDeCasilla();
		assertEquals(Color.BLANCO, p.getColor());
		assertEquals('P', p.getTipo());
	}
		

    @Test(expected=ExcepcionPiezaDesconocida.class)
    public final void testColocaPieza_GetPiezaAt2() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
            paj.colocaPieza("Xb F3");
    }


	
	@Test
	public final void testAddMovimiento() throws ExcepcionCoordenadaErronea, ExcepcionNoExisteMovimiento {
		Coordenada co = new Coordenada('E',2);
		Coordenada cd = new Coordenada('E',3);
		paj.addMovimiento(co, cd);
		Movimiento mov = paj.getMovimientoAt(0);
		assertEquals(co,mov.getCoordenadaOrigen());
		assertEquals(cd,mov.getCoordenadaDestino());
		assertEquals(1,paj.getNumMovimientos());
	}

}
