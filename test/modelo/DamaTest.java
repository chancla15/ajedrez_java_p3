/**
 * 
 */
package modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimiento;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionPiezaDesconocida;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DamaTest {

	Pieza d0, dn, pn, db, pb, pb2, pb3;
	Casilla c0, a2, a3, a4, a5, b1, b2, b3, b4, b5, b6, b7, c1, c2, c3, c4, c6, c7, d1, d2, d5;
	Casilla e1, e2, e4, e6, a1, h1, h2, e8, a8, h3, h8, g1, g2, g3, g5, g6, g7, f1, f2, f4, f7;
	Casilla d6, h5, d3, f3, d7, c5, d4, c8, a7;
	Pieza tb, tn, tb2;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situaciï¿½n del juego (puedeMover() consulta el
	// tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		d0 = new Dama(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		db = new Dama(Color.BLANCO);
		dn = new Dama(Color.NEGRO);
		// P4
		tb = new Torre(Color.BLANCO);
		tb2 = new Torre(Color.BLANCO);
		tn = new Torre(Color.NEGRO);
		pb2 = new Peon(Color.BLANCO);
		pb3 = new Peon(Color.BLANCO);

		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A', 2);
		a3 = tablero.getCasillaAt('A', 3);
		a4 = tablero.getCasillaAt('A', 4);
		a5 = tablero.getCasillaAt('A', 5);
		b1 = tablero.getCasillaAt('B', 1);
		b2 = tablero.getCasillaAt('B', 2);
		b3 = tablero.getCasillaAt('B', 3);
		b4 = tablero.getCasillaAt('B', 4);
		b5 = tablero.getCasillaAt('B', 5);
		b6 = tablero.getCasillaAt('B', 6);
		b7 = tablero.getCasillaAt('B', 7);
		c2 = tablero.getCasillaAt('C', 2);
		c3 = tablero.getCasillaAt('C', 3);
		c4 = tablero.getCasillaAt('C', 4);
		// P4
		a1 = tablero.getCasillaAt('A', 1);
		e1 = tablero.getCasillaAt('E', 1);
		e2 = tablero.getCasillaAt('E', 2);
		e4 = tablero.getCasillaAt('E', 4);
		h1 = tablero.getCasillaAt('H', 1);
		h2 = tablero.getCasillaAt('H', 2);
		h3 = tablero.getCasillaAt('H', 3);
		a8 = tablero.getCasillaAt('A', 8);
		e8 = tablero.getCasillaAt('E', 8);
		h8 = tablero.getCasillaAt('H', 8);
		h5 = tablero.getCasillaAt('H', 5);
		c1 = tablero.getCasillaAt('C', 1);
		c6 = tablero.getCasillaAt('C', 6);
		c7 = tablero.getCasillaAt('C', 7);
		d1 = tablero.getCasillaAt('D', 1);
		d2 = tablero.getCasillaAt('D', 2);
		d3 = tablero.getCasillaAt('D', 3);
		d7 = tablero.getCasillaAt('D', 7);
		d5 = tablero.getCasillaAt('D', 5);
		d6 = tablero.getCasillaAt('D', 6);
		e6 = tablero.getCasillaAt('E', 6);
		f1 = tablero.getCasillaAt('F', 1);
		f2 = tablero.getCasillaAt('F', 2);
		f3 = tablero.getCasillaAt('F', 3);
		f4 = tablero.getCasillaAt('F', 4);
		f7 = tablero.getCasillaAt('F', 7);
		g1 = tablero.getCasillaAt('G', 1);
		g2 = tablero.getCasillaAt('G', 2);
		g3 = tablero.getCasillaAt('G', 3);
		g5 = tablero.getCasillaAt('G', 5);
		g6 = tablero.getCasillaAt('G', 6);
		g7 = tablero.getCasillaAt('G', 7);
		c5 = tablero.getCasillaAt('C', 5);
		d4 = tablero.getCasillaAt('D', 4);
		c8 = tablero.getCasillaAt('C', 8);
		a7 = tablero.getCasillaAt('A', 7);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		d0.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		db.quitaDeCasilla();
		dn.quitaDeCasilla();
		tb.quitaDeCasilla();
		tn.quitaDeCasilla();
		pb2.quitaDeCasilla();
		pb3.quitaDeCasilla();
		tb2.quitaDeCasilla();
	}

	/**
	 * Test method for {@link modelo.Caballo#isMismoTipo(modelo.Pieza)}.
	 */
	@Test
	public final void testIsMismoTipo() {
		assertTrue(db.isMismoTipo(dn));
		assertFalse(db.isMismoTipo(pb));
	}

	/**
	 * Test method for {@link modelo.Caballo#getTipo()}.
	 */
	@Test
	public final void testGetTipo() {
		assertEquals('D', db.getTipo());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas() {
		db.setCasilla(a1);
		Set<Casilla> set = db.getCasillasAmenazadas();
		assertTrue(set.contains(b2));
		assertTrue(set.contains(c3));
		assertTrue(set.contains(h8));
		assertFalse(set.contains(a1));
		assertEquals(21, set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas2() {
		db.setCasilla(a8);
		Set<Casilla> set = db.getCasillasAmenazadas();
		assertTrue(set.contains(b7));
		assertTrue(set.contains(h1));
		assertEquals(21, set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas3() {
		db.setCasilla(h8);
		Set<Casilla> set = db.getCasillasAmenazadas();
		assertTrue(set.contains(g7));
		assertTrue(set.contains(a1));
		assertEquals(21, set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas4() {
		db.setCasilla(h1);
		Set<Casilla> set = db.getCasillasAmenazadas();
		assertTrue(set.contains(g2));
		assertTrue(set.contains(a8));
		assertEquals(21, set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas5() {
		db.setCasilla(d5);
		Set<Casilla> set = db.getCasillasAmenazadas();
		assertTrue(set.contains(c4));
		assertTrue(set.contains(a2));
		assertTrue(set.contains(e6));
		assertTrue(set.contains(f7));
		assertTrue(set.contains(c6));
		assertTrue(set.contains(a8));
		assertTrue(set.contains(e4));
		assertTrue(set.contains(h1));

		assertEquals(27, set.size());
	}

	/**
	 * Comprueba casilla amenazadas con piezas interpuestas
	 */
	@Test
	public final void testGetCasillasAmenazadas6() {
		db.setCasilla(d5);
		dn.setCasilla(b7);
		pb.setCasilla(a2);
		pn.setCasilla(e6);
		pb2.setCasilla(g2);
		tb.setCasilla(d6);
		tn.setCasilla(h5);
		tn.setCasilla(h5);
		tn.setCasilla(h5);
		tn.setCasilla(h5);
		Set<Casilla> set = db.getCasillasAmenazadas();
		assertTrue(set.contains(c4));
		assertTrue(set.contains(b3));
		assertTrue(set.contains(a2));
		assertTrue(set.contains(e6));
		assertTrue(set.contains(d6));
		assertTrue(set.contains(c6));
		assertTrue(set.contains(b7));
		assertTrue(set.contains(d3));
		assertTrue(set.contains(f3));
		assertTrue(set.contains(g2));
		assertTrue(set.contains(d2));
		assertTrue(set.contains(a5));
		assertFalse(set.contains(h1));
		assertFalse(set.contains(d7));
		assertFalse(set.contains(c7));

		assertEquals(21, set.size());
	}

	/**
	 * Puede mover sin piezas interpuestas
	 */
	@Test
	public final void testPuedeMover() {
		db.setCasilla(c3);
		assertTrue(db.puedeMover(a1));
		assertTrue(db.puedeMover(a5));
		assertTrue(db.puedeMover(e1));
		assertTrue(db.puedeMover(b4));
		assertTrue(db.puedeMover(d4));
		assertTrue(db.puedeMover(h8));
		assertTrue(db.puedeMover(e1));
		assertTrue(db.puedeMover(c5));

		assertFalse(db.puedeMover(c3));
		assertFalse(db.puedeMover(d5));
		assertFalse(db.puedeMover(h1));
		assertFalse(db.puedeMover(h2));
	}

	/**
	 * Puede mover a casilla ocupada por el contrario, pero no por pieza del mismo color
	 */
	@Test
	public final void testPuedeMover2() {
		db.setCasilla(c3);
		pb.setCasilla(a5);
		pn.setCasilla(e1);
		assertFalse(db.puedeMover(a5));
		assertTrue(db.puedeMover(e1));
		tb.setCasilla(c8);
		tn.setCasilla(h3);
		assertFalse(db.puedeMover(c8));
		assertTrue(db.puedeMover(h3));
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * 
	 * @throws ExcepcionMovimientoIlegal
	 * @throws ExcepcionCasillaDestinoOcupada
	 * 
	 *             Comprueba haMovido() para la dama
	 */
	@Test
	public final void testMueve0() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		assertFalse(db.haMovido());
		db.setCasilla(c3);
		try {
			db.mueve(b1);
		} catch (ExcepcionMovimiento ex) {
			// nada
		}
		assertFalse(db.haMovido());
		db.mueve(c8);
		assertTrue(db.haMovido());
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * 
	 * @throws ExcepcionMovimientoIlegal
	 * @throws ExcepcionCasillaDestinoOcupada
	 * 
	 *             Comprueba que la dama mueve.
	 */
	@Test
	public final void testMueve() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		db.setCasilla(c3);
		db.mueve(a1);
		assertSame(a1, db.getCasilla());
		assertNull(c3.getPieza());
		db.mueve(a7);
		assertSame(a7, db.getCasilla());
		assertNull(a1.getPieza());
		db.mueve(d4);
		assertSame(d4, db.getCasilla());
		assertNull(a7.getPieza());
		db.mueve(g7);
		assertSame(g7, db.getCasilla());
		assertNull(d4.getPieza());
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * 
	 * @throws ExcepcionMovimientoIlegal
	 * @throws ExcepcionCasillaDestinoOcupada
	 * 
	 *             Comprueba movimiento ilegal
	 */
	@Test(expected = ExcepcionMovimientoIlegal.class)
	public final void testMueve2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		db.setCasilla(c3);
		db.mueve(f7);
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * 
	 * @throws ExcepcionMovimientoIlegal
	 * @throws ExcepcionCasillaDestinoOcupada
	 */
	@Test(expected = ExcepcionCasillaDestinoOcupada.class)
	public final void testMueve3() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		db.setCasilla(c3);
		pb.setCasilla(a1);
		db.mueve(a1);
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * 
	 * @throws ExcepcionMovimientoIlegal
	 * @throws ExcepcionCasillaDestinoOcupada
	 * 
	 *             Comprueba que la dama captura
	 */
	@Test
	public final void testCaptura() throws ExcepcionCasillaDestinoOcupada,
			ExcepcionMovimientoIlegal {
		db.setCasilla(d5);
		pn.setCasilla(f7);
		db.mueve(f7);
		assertSame(f7, db.getCasilla());
		assertNull(d5.getPieza());
		assertNull(pn.getCasilla());
		tn.setCasilla(a7);
		db.mueve(a7);
		assertSame(a7, db.getCasilla());
		assertNull(f7.getPieza());
		assertNull(tn.getCasilla());

		dn.setCasilla(d5);
		pb.setCasilla(f7);
		dn.mueve(f7);
		assertSame(f7, dn.getCasilla());
		assertNull(d5.getPieza());
		assertNull(pb.getCasilla());
		dn.mueve(a7);
		assertSame(a7, dn.getCasilla());
		assertNull(f7.getPieza());
		assertNull(db.getCasilla());
	}

	@Test
	public final void testMetaclass() throws ExcepcionPiezaDesconocida {
		Pieza t = FactoriaPieza.creaPieza('D', Color.BLANCO);
		assertSame(dn.getClass(), t.getClass());
	}

}
