/**
 * 
 */
package modelo;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimiento;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author pierre
 *
 */
public class EvaluadorAjedrezPorPosicionTest {

	 static PartidaAjedrez paj;
	 static Tablero tablero;
/*	 Pieza[] pb;
	 Pieza[] pn;
	 Pieza Db, Dn, Rb, Rn;
*/
	 
	 @BeforeClass
	 public static void setUpBeforeClass() throws Exception {
		 paj = PartidaAjedrez.getInstancia();
		 tablero=paj.getTablero();
	 }
	/**
	 * @throws java.lang.Exception
	 */
/*	@Before
	public final void setUpBefore() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();
		pb = new Pieza[9];
		pn = new Pieza[9];
		for (int i=0; i<9; i++) {
			pb[i] = FactoriaPieza.creaPieza('P', Color.BLANCO);
			pn[i] = FactoriaPieza.creaPieza('P', Color.NEGRO);
		}
		Db = FactoriaPieza.creaPieza('D', Color.BLANCO);
		Dn = FactoriaPieza.creaPieza('D', Color.NEGRO);
		Rb = FactoriaPieza.creaPieza('R', Color.BLANCO);
		Rn = FactoriaPieza.creaPieza('R', Color.NEGRO);
	}*/

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public final void tearDownAfter() throws Exception {
		Iterator<Pieza> itb = paj.getPiezas(Color.BLANCO).iterator();
		Iterator<Pieza> itn = paj.getPiezas(Color.NEGRO).iterator();
		
		while(itb.hasNext())
			itb.next().quitaDeCasilla();
		while(itn.hasNext())
			itn.next().quitaDeCasilla();		
	}

	/**
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evalua(modelo.PartidaAjedrez, modelo.Color)}.
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testEvaluaAmenazas() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		paj.colocaPieza("Rb E1");
		paj.colocaPieza("Db C2");
		paj.colocaPieza("Pb E3");
		paj.colocaPieza("Rn E8");
		paj.colocaPieza("Dn C7");
		paj.colocaPieza("Pn E6");
		
		// blancas y negras
		// casillas amenazadas = 25
		// piezas defendidas   =  0
		// Peones avanzados    =  0
		// Peones doblados     =  0
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorPosicion();
		assertEquals(25.0, eva.evalua(paj, Color.BLANCO), 0.001);
		assertEquals(25.0, eva.evalua(paj, Color.NEGRO), 0.001);
	}

	/**
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evalua(modelo.PartidaAjedrez, modelo.Color)}.
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testEvaluaAmenazas2() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		paj.colocaPieza("Rb E1");
		paj.colocaPieza("Db D1");
		paj.colocaPieza("Pb E2");
		paj.colocaPieza("Rn E8");
		paj.colocaPieza("Dn D8");
		paj.colocaPieza("Pn E7");
		
		// blancas y negras
		// casillas amenazadas = 19
		// piezas defendidas   =  3
		// Peones avanzados    =  0
		// Peones doblados     =  0
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorPosicion();
		assertEquals(22.0, eva.evalua(paj, Color.BLANCO), 0.001);
		assertEquals(22.0, eva.evalua(paj, Color.NEGRO), 0.001);
	}

	/**
	 * Comprueba Peones doblados
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evalua(modelo.PartidaAjedrez, modelo.Color)}.
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testEvaluaPeonesDoblados() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		paj.colocaPieza("Pb A2");
		paj.colocaPieza("Pb A3");
		paj.colocaPieza("Pb A4");
		paj.colocaPieza("Pb E2");
		paj.colocaPieza("Pb E3");
		paj.colocaPieza("Pb E4");

		paj.colocaPieza("Pn A5");
		paj.colocaPieza("Pn A6");
		paj.colocaPieza("Pn A7");
		paj.colocaPieza("Pn E5");
		paj.colocaPieza("Pn E6");
		paj.colocaPieza("Pn E7");

		// blancas y negras
		// casillas amenazadas =  9
		// piezas defendidas   =  0
		// Peones avanzados    =  0
		// Peones doblados     = -4
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorPosicion();
		assertEquals(5.0, eva.evalua(paj, Color.BLANCO), 0.001);
		assertEquals(5.0, eva.evalua(paj, Color.NEGRO), 0.001);
	}
	
	/**
	 * Comprueba Peones doblados
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evalua(modelo.PartidaAjedrez, modelo.Color)}.
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testEvaluaPeonesAvanzados1() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		paj.colocaPieza("Pb A5");
		paj.colocaPieza("Pb B5");
		paj.colocaPieza("Pb D4");
		paj.colocaPieza("Pb E4");
		paj.colocaPieza("Pb G5");
		paj.colocaPieza("Pb H5");

		paj.colocaPieza("Pn A4");
		paj.colocaPieza("Pn B4");
		paj.colocaPieza("Pn D5");
		paj.colocaPieza("Pn E5");
		paj.colocaPieza("Pn G4");
		paj.colocaPieza("Pn H4");

		// blancas y negras
		// casillas amenazadas = 10
		// piezas defendidas   =  0
		// Peones avanzados    =  4
		// Peones doblados     =  0
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorPosicion();
		assertEquals(14.0, eva.evalua(paj, Color.BLANCO), 0.001);
		assertEquals(14.0, eva.evalua(paj, Color.NEGRO), 0.001);
	}
	
	/**
	 * Comprueba Peones doblados
	 * Test method for {@link modelo.EvaluadorAjedrezPorValorPiezas#evalua(modelo.PartidaAjedrez, modelo.Color)}.
	 * @throws ExcepcionPosicionNoValida 
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionPiezaDesconocida 
	 */
	@Test
	public final void testEvaluaPeonesAvanzados2() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		paj.colocaPieza("Pb A6");
		paj.colocaPieza("Pb B6");
		paj.colocaPieza("Pb D4");
		paj.colocaPieza("Pb E4");
		paj.colocaPieza("Pb G6");
		paj.colocaPieza("Pb H6");

		paj.colocaPieza("Pn A3");
		paj.colocaPieza("Pn B3");
		paj.colocaPieza("Pn D5");
		paj.colocaPieza("Pn E5");
		paj.colocaPieza("Pn G3");
		paj.colocaPieza("Pn H3");

		// blancas y negras
		// casillas amenazadas = 10
		// piezas defendidas   =  0
		// Peones avanzados    =  8
		// Peones doblados     =  0
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorPosicion();
		assertEquals(18.0, eva.evalua(paj, Color.BLANCO), 0.001);
		assertEquals(18.0, eva.evalua(paj, Color.NEGRO), 0.001);
	}
	
	@Test
	public final void testEvaluaPosicionInicial() throws FileNotFoundException {
		paj.setEntrada("test/sinpos_sinmov.ent");
		paj.setSalida("test/sinpos_sinmov.out");
		paj.run();

		// blancas y negras
		// casillas amenazadas = 22
		// piezas defendidas   = 14
		// Peones avanzados    =  0
		// Peones doblados     =  0
		EvaluadorAjedrez eva = new EvaluadorAjedrezPorPosicion();
		assertEquals(36.0, eva.evalua(paj, Color.BLANCO), 0.001);
		assertEquals(36.0, eva.evalua(paj, Color.NEGRO), 0.001);
		
	}

}
