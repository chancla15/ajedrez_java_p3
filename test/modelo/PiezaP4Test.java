/**
 * 
 */
package modelo;

import static org.junit.Assert.*;

import java.util.Iterator;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimientoIlegal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author pierre
 *
 */
public class PiezaP4Test {

	Pieza p,t,c,a,d,r;
	PartidaAjedrez paj;
	Tablero tablero;
	Casilla e2,e3;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();
		p = FactoriaPieza.creaPieza('P', Color.BLANCO);
		t = FactoriaPieza.creaPieza('T', Color.BLANCO);
		c = FactoriaPieza.creaPieza('C', Color.BLANCO);
		a = FactoriaPieza.creaPieza('A', Color.BLANCO);
		d = FactoriaPieza.creaPieza('D', Color.BLANCO);
		r = FactoriaPieza.creaPieza('R', Color.BLANCO);
		e2 = tablero.getCasillaAt('E', 2);
		e3 = tablero.getCasillaAt('E', 3);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		//P4
		Iterator<Pieza> itb = paj.getPiezas(Color.BLANCO).iterator();
		Iterator<Pieza> itn = paj.getPiezas(Color.NEGRO).iterator();
		
		while(itb.hasNext())
			itb.next().quitaDeCasilla();
		while(itn.hasNext())
			itn.next().quitaDeCasilla();		

	}

	/**
	 * Test method for {@link modelo.Pieza#setColor(modelo.Color)}.
	 */
	@Test
	public final void testSetColor() {
		p.setColor(Color.NEGRO);
		assertEquals(Color.NEGRO,p.getColor());
	}

	/**
	 * Test method for {@link modelo.Pieza#haMovido()}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 */
	@Test
	public final void testHaMovido() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		p.setCasilla(e2);
		assertFalse(p.haMovido());
		p.mueve(e3);
		assertTrue(p.haMovido());
		p.quitaDeCasilla();
	}

	/**
	 * Test method for {@link modelo.Pieza#getValor()}.
	 */
	@Test
	public final void testGetValor() {
		assertEquals(1.0,p.getValor(),0.001);
		assertEquals(5.0,t.getValor(),0.001);
		assertEquals(3.0,c.getValor(),0.001);
		assertEquals(3.0,a.getValor(),0.001);
		assertEquals(9.0,d.getValor(),0.001);
		assertEquals(0.0,r.getValor(),0.001);
	}

}
