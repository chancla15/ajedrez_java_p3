/**
 * 
 */
package modelo;

import static org.junit.Assert.*;

import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimiento;
import modelo.excepciones.ExcepcionMovimientoIlegal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author pierre
 *
 */
public class CaballoTest {

	Pieza ca0, cn, pn, cb, pb, pb2, pb3;
	Casilla c0,a2,a3,a4,a5,b1,b2,b3,b4,b5,b6,b7,c1,c2,c3,c4,c7,d1,d2,d5;
	Casilla e1,e2,e4,a1,h1,h2,e8,a8,h3,h8,g1,g2,g3,g5,g6,f1,f2,f4,f7;
	//Pieza tb,tn,tb2;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situación del juego (puedeMover() consulta el tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		ca0 = new Caballo(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		cn = new Caballo(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		cb = new Caballo(Color.BLANCO);
		//P4
		pb2 = new Peon(Color.BLANCO);
		pb3 = new Peon(Color.BLANCO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		a5 = tablero.getCasillaAt('A',5);
		b1 = tablero.getCasillaAt('B',1);
		b2 = tablero.getCasillaAt('B',2);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		c2 = tablero.getCasillaAt('C',2);
		c3 = tablero.getCasillaAt('C',3);
		c4 = tablero.getCasillaAt('C',4);
		// P4
		a1 = tablero.getCasillaAt('A',1);
		e1 = tablero.getCasillaAt('E',1);
		e2 = tablero.getCasillaAt('E',2);
		e4 = tablero.getCasillaAt('E',4);
		h1 = tablero.getCasillaAt('H',1);
		h2 = tablero.getCasillaAt('H',2);
		h3 = tablero.getCasillaAt('H',3);
		a8 = tablero.getCasillaAt('A',8);
		e8 = tablero.getCasillaAt('E',8);
		h8 = tablero.getCasillaAt('H',8);
		c1 = tablero.getCasillaAt('C',1);
		c7 = tablero.getCasillaAt('C',7);
		d1 = tablero.getCasillaAt('D',1);
		d2 = tablero.getCasillaAt('D',2);
		d5 = tablero.getCasillaAt('D',5);
		f1 = tablero.getCasillaAt('F',1);
		f2 = tablero.getCasillaAt('F',2);
		f4 = tablero.getCasillaAt('F',4);
		f7 = tablero.getCasillaAt('F',7);
		g1 = tablero.getCasillaAt('G',1);
		g2 = tablero.getCasillaAt('G',2);
		g3 = tablero.getCasillaAt('G',3);
		g5 = tablero.getCasillaAt('G',5);
		g6 = tablero.getCasillaAt('G',6);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		ca0.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		cb.quitaDeCasilla();
		cn.quitaDeCasilla();
//		tb.quitaDeCasilla();
//		tn.quitaDeCasilla();
		pb2.quitaDeCasilla();
		pb3.quitaDeCasilla();
//		tb2.quitaDeCasilla();
	}

	/**
	 * Test method for {@link modelo.Caballo#isMismoTipo(modelo.Pieza)}.
	 */
	@Test
	public final void testIsMismoTipo() {
		assertTrue(cb.isMismoTipo(cn));
		assertFalse(cb.isMismoTipo(pb));
	}

	/**
	 * Test method for {@link modelo.Caballo#getTipo()}.
	 */
	@Test
	public final void testGetTipo() {
		assertEquals('C',cb.getTipo());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas() {
		cb.setCasilla(a1);
		Set<Casilla> set = cb.getCasillasAmenazadas();
		assertTrue(set.contains(b3));
		assertTrue(set.contains(c2));
		assertEquals(2,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas2() {
		cb.setCasilla(a8);
		Set<Casilla> set = cb.getCasillasAmenazadas();
		assertTrue(set.contains(b6));
		assertTrue(set.contains(c7));
		assertEquals(2,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas3() {
		cb.setCasilla(h1);
		Set<Casilla> set = cb.getCasillasAmenazadas();
		assertTrue(set.contains(f2));
		assertTrue(set.contains(g3));
		assertEquals(2,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas4() {
		cb.setCasilla(h8);
		Set<Casilla> set = cb.getCasillasAmenazadas();
		assertTrue(set.contains(f7));
		assertTrue(set.contains(g6));
		assertEquals(2,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas5() {
		cb.setCasilla(a3);
		Set<Casilla> set = cb.getCasillasAmenazadas();
		assertTrue(set.contains(b1));
		assertTrue(set.contains(c2));
		assertTrue(set.contains(b5));
		assertTrue(set.contains(c4));
		assertEquals(4,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas6() {
		cb.setCasilla(h3);
		Set<Casilla> set = cb.getCasillasAmenazadas();
		assertTrue(set.contains(g1));
		assertTrue(set.contains(g5));
		assertTrue(set.contains(f2));
		assertTrue(set.contains(f4));
		assertEquals(4,set.size());
	}

	/**
	 * Test method for {@link modelo.Caballo#getCasillasAmenazadas()}.
	 */
	@Test
	public final void testGetCasillasAmenazadas7() {
		cb.setCasilla(c3);
		Set<Casilla> set = cb.getCasillasAmenazadas();
		assertTrue(set.contains(a2));
		assertTrue(set.contains(a4));
		assertTrue(set.contains(b1));
		assertTrue(set.contains(b5));
		assertTrue(set.contains(d1));
		assertTrue(set.contains(d5));
		assertTrue(set.contains(e2));
		assertTrue(set.contains(e4));
		assertEquals(8,set.size());
	}

	/**
	 * Test method for {@link modelo.Pieza#puedeMover(modelo.Casilla)}.
	 */
	@Test
	public final void testPuedeMover() {
		cb.setCasilla(c3);
		assertTrue(cb.puedeMover(a2));
		assertTrue(cb.puedeMover(a4));
		assertTrue(cb.puedeMover(b1));
		assertTrue(cb.puedeMover(b5));
		assertTrue(cb.puedeMover(d1));
		assertTrue(cb.puedeMover(d5));
		assertTrue(cb.puedeMover(e2));
		assertTrue(cb.puedeMover(e4));

		assertFalse(cb.puedeMover(c3));
		assertFalse(cb.puedeMover(c4));
		assertFalse(cb.puedeMover(f7));
		
	}

	/**
	 * Test method for {@link modelo.Pieza#puedeMover(modelo.Casilla)}.
	 * Comprueba que el caballo puede mover a casillas ocupadas por el contrario, pero no por pieza del mismo color
	 */
	@Test
	public final void testPuedeMover2() {
		cb.setCasilla(c3);
		pb.setCasilla(a2);
		pn.setCasilla(a4);
		assertFalse(cb.puedeMover(a2));
		assertTrue(cb.puedeMover(a4));
		
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 */
	@Test
	public final void testMueve0() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		assertFalse(cb.haMovido());
		cb.setCasilla(c3);
		try {
			cb.mueve(c2);
		} catch (ExcepcionMovimiento ex) {
		// nada	
		}
		assertFalse(cb.haMovido());
		cb.mueve(a2);
		assertTrue(cb.haMovido());
	}
 
	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 * Comprueba captura
	 */
	@Test
	public final void testMueveCaptura() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		cb.setCasilla(c3);
		pn.setCasilla(a2);
		cb.mueve(a2);
		assertSame(a2,cb.getCasilla());
		assertNull(c3.getPieza());
		assertNull(pn.getCasilla());
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 * Comprueba caballo no mueve si pieza en destino es del mismo color
	 */
	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testMueveNoCaptura() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		cb.setCasilla(c3);
		pb.setCasilla(a2);
		cb.mueve(a2);
	}

	/**
	 * Test method for {@link modelo.Pieza#mueve(modelo.Casilla)}.
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 */
	@Test(expected=ExcepcionMovimientoIlegal.class)
	public final void testMueve2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		cb.setCasilla(c3);
		cb.mueve(f7);
	}

	
}
