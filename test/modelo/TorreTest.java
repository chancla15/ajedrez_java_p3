package modelo;

import static org.junit.Assert.*;

import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionPiezaDesconocida;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TorreTest {

	Pieza r0, rn, pn, rb, pb;
	Casilla c0,a2,a3,a4,a5,b3,b4,b5,b6,b7,c4;
	Casilla e1,a1,h1,e8,a8,h8,b1,c1,d1,d2,f1,g1;
	Pieza tb,tn;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situaciï¿½n del juego (puedeMover() consulta el tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	@Before
	public void setUp() throws Exception {
		
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		r0 = new Rey(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		rn = new Rey(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		rb = new Rey(Color.BLANCO);
		//P4
		tb = new Torre(Color.BLANCO);
		tn = new Torre(Color.NEGRO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		a5 = tablero.getCasillaAt('A',5);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		c4 = tablero.getCasillaAt('C',4);
		// P4
		a1 = tablero.getCasillaAt('A',1);
		b1 = tablero.getCasillaAt('B',1);
		c1 = tablero.getCasillaAt('C',1);
		d1 = tablero.getCasillaAt('D',1);
		d2 = tablero.getCasillaAt('D',2);
		e1 = tablero.getCasillaAt('E',1);
		f1 = tablero.getCasillaAt('F',1);
		g1 = tablero.getCasillaAt('G',1);
		h1 = tablero.getCasillaAt('H',1);
		a8 = tablero.getCasillaAt('A',8);
		e8 = tablero.getCasillaAt('E',8);
		h8 = tablero.getCasillaAt('H',8);

	}

	@After
	public void tearDown() {
		r0.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		rb.quitaDeCasilla();
		rn.quitaDeCasilla();
		tb.quitaDeCasilla();
		tn.quitaDeCasilla();
	}
	
	@Test
	public final void testPuedeMover() {
		tb.setCasilla(a1);
		assertTrue(tb.puedeMover(a2));
		assertTrue(tb.puedeMover(a8));
		assertTrue(tb.puedeMover(e1));
		assertTrue(tb.puedeMover(h1));
		rb.setCasilla(e1);
		assertFalse(tb.puedeMover(e1));
		assertFalse(tb.puedeMover(h1));
		rn.setCasilla(a4);
		assertTrue(tb.puedeMover(a4));
		assertFalse(tb.puedeMover(a8));
	}

	@Test
	public final void testGetCasillasAmenazadas() {
		tb.setCasilla(a1);
		Set<Casilla> set = tb.getCasillasAmenazadas();
		assertTrue(set.contains(a2));
		assertTrue(set.contains(a8));
		assertTrue(set.contains(e1));
		assertTrue(set.contains(h1));
		rb.setCasilla(e1);
		set = tb.getCasillasAmenazadas();
		assertTrue(set.contains(e1));
		assertFalse(set.contains(h1));
		rn.setCasilla(a4);
		set = tb.getCasillasAmenazadas();
		assertTrue(set.contains(a2));
		assertTrue(set.contains(a4));
		assertFalse(set.contains(a5));
		assertFalse(set.contains(a8));
	}
	
	@Test
	public final void testGetCasillasAmenazadas2() {
		tb.setCasilla(a1);
		Set<Casilla> set = tb.getCasillasAmenazadas();
		assertEquals (set.size(), 14);
		rn.setCasilla(a4);
		set = tb.getCasillasAmenazadas();
		assertEquals (set.size(), 10);
	}

	@Test
	public final void testGetCasillasAmenazadas3() {
		tb.setCasilla(a1);
		rn.setCasilla(a4);
		Set<Casilla> set = tb.getCasillasAmenazadas();
		assertTrue(set.contains(a2));
		assertTrue(set.contains(a3));
		assertTrue(set.contains(a4));
		assertTrue(set.contains(b1));
		assertTrue(set.contains(c1));
		assertTrue(set.contains(d1));
		assertTrue(set.contains(e1));
		assertTrue(set.contains(f1));
		assertTrue(set.contains(g1));
		assertTrue(set.contains(h1));

		assertFalse(set.contains(a5));
	}

	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testHaMovido() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		assertFalse(tb.haMovido());
		tb.setCasilla(e1);
		assertFalse(tb.haMovido());
		tb.mueve(e1);
		assertFalse(tb.haMovido());
	}

	@Test(expected = ExcepcionMovimientoIlegal.class)
	public final void testMovimientoIlegal() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		tb.setCasilla(a1);
		tb.mueve(e8);
	}

	@Test //(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testHaMovido2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		assertFalse(tb.haMovido());
		assertNull(tb.getCasilla());
		tb.setCasilla(e1);
		assertNotNull(tb.getCasilla());
		assertFalse(tb.haMovido());
		tb.mueve(e8);
		assertEquals(tb.getCasilla(),e8);
		assertTrue(tb.haMovido());
	}

	@Test
	public final void testSetColor() {
		tb.setColor(Color.NEGRO);
		assertEquals(Color.NEGRO,tb.getColor());
		tb.setColor(Color.BLANCO);
		assertEquals(Color.BLANCO,tb.getColor());
	}

	@Test
	public final void testMetaclass() throws ExcepcionPiezaDesconocida  {
		Pieza t = FactoriaPieza.creaPieza('T', Color.BLANCO);
		assertSame(tn.getClass(), t.getClass());
	}
	
	/*@Test(expected=ExcepcionPiezaDesconocida.class)
	public final void testExceptionPiezaDesconocida() throws ExcepcionPiezaDesconocida  {
		Pieza t = FactoriaPieza.creaPieza('t', Color.BLANCO);
		assertEquals(t,null);
	}*/
	
	@Test
	public final void testCasilla() throws ExcepcionCoordenadaErronea {
		tn.setCasilla(b4);
		Casilla aux = tablero.getCasillaAt('B',4);
		assertEquals(tn.getCasilla(),aux);
		assertSame(tn.getCasilla(),aux);
	}

	@Test
	public final void testIsMismoTipo() {
		assertTrue(tb.isMismoTipo(tn));
		assertFalse(tb.isMismoTipo(pb));
	}

	@Test
	public final void testGetTipo() {
		assertEquals('T', tb.getTipo());
	}

	@Test
	public final void testGetTipo2() {
		char ttb = tb.getTipo();
		char ttn = tn.getTipo();
		assertEquals(ttb,ttn);
	}

	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testCaptura() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		tb.setCasilla(d1);
		pn.setCasilla(d2);
		tb.mueve(d2);
		assertSame(d2,tb.getCasilla());
		assertNull(d1.getPieza());
		assertNull(pn.getCasilla());

		pb.setCasilla(d1);
		tb.mueve(d1);
		assertSame(d2,tb.getCasilla());
		assertSame(d1,pb.getCasilla());
	}
}
