/**
 * 
 */
package modelo;

import static org.junit.Assert.*;

import java.util.Iterator;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author pierre
 *
 */
public class CasillaP4Test {

	Casilla c1;
	Casilla c2;
	Casilla c3;
	Casilla c4;
	
	PartidaAjedrez paj;
	Tablero tablero;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		c1 = new Casilla(Color.BLANCO,new Coordenada('H',1));
		c2 = new Casilla(Color.BLANCO,new Coordenada('H',1));
		c3 = new Casilla(Color.NEGRO,new Coordenada('H',1));
		c4 = new Casilla(Color.BLANCO,new Coordenada('H',2));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		Iterator<Pieza> itb = paj.getPiezas(Color.BLANCO).iterator();
		Iterator<Pieza> itn = paj.getPiezas(Color.NEGRO).iterator();
		
		while(itb.hasNext())
			itb.next().quitaDeCasilla();
		while(itn.hasNext())
			itn.next().quitaDeCasilla();		

	}

	/**
	 * Test method for {@link modelo.Casilla#equals(java.lang.Object)}.
	 */
	@Test
	public final void testEqualsObject() {
		assertEquals(c1,c2);
		assertFalse(c1.equals(c3));
		assertFalse(c1.equals(c4));
	}

	/**
	 * Test method for {@link modelo.Casilla#isAmenazada(modelo.Color)}.
	 * @throws ExcepcionCoordenadaErronea 
	 * @throws ExcepcionPiezaDesconocida 
	 * @throws ExcepcionMovimientoIlegal 
	 * @throws ExcepcionCasillaDestinoOcupada 
	 * @throws ExcepcionPosicionNoValida 
	 */
	@Test
	public final void testIsAmenazada() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal, ExcepcionPosicionNoValida {
		Casilla d2 = tablero.getCasillaAt('D', 2);
		Casilla c3 = tablero.getCasillaAt('C', 3);
		Casilla d8 = tablero.getCasillaAt('D', 8);
		Casilla e8 = tablero.getCasillaAt('E', 8);

		assertFalse(d2.isAmenazada(Color.BLANCO));
		assertFalse(d2.isAmenazada(Color.NEGRO));
		
		paj.colocaPieza("Dn D8");
		assertFalse(d2.isAmenazada(Color.BLANCO));
		assertTrue(d2.isAmenazada(Color.NEGRO));
		
		d8.getPieza().mueve(e8);
		assertFalse(d2.isAmenazada(Color.BLANCO));
		assertFalse(d2.isAmenazada(Color.NEGRO));
		
		paj.colocaPieza("Ab C3");
		assertTrue(d2.isAmenazada(Color.BLANCO));
		assertFalse(d2.isAmenazada(Color.NEGRO));

		e8.getPieza().quitaDeCasilla();
		c3.getPieza().quitaDeCasilla();
	}

}
