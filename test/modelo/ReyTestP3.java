package modelo;

import static org.junit.Assert.*;

import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimientoIlegal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ReyTestP3 {

	Pieza r0, rn, pn, rb, pb, pb2, pb3;
	Casilla c0,a2,a3,a4,a5,b1,b2,b3,b4,b5,b6,b7,c1,c2,c3,c4,c8,d1,d2,d8;
	Casilla e1,a1,h1,h2,e8,a8,h8,g1,g2,g8,f1,f2,f8;
	Pieza tb,tn,tb2;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situación del juego (puedeMover() consulta el tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	@Before
	public void setUp() throws Exception {
		
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		r0 = new Rey(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		rn = new Rey(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		rb = new Rey(Color.BLANCO);
		//P4
		tb = new Torre(Color.BLANCO);
		tb2 = new Torre(Color.BLANCO);
		tn = new Torre(Color.NEGRO);
		pb2 = new Peon(Color.BLANCO);
		pb3 = new Peon(Color.BLANCO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		a5 = tablero.getCasillaAt('A',5);
		b1 = tablero.getCasillaAt('B',1);
		b2 = tablero.getCasillaAt('B',2);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		c2 = tablero.getCasillaAt('C',2);
		c3 = tablero.getCasillaAt('C',3);
		c4 = tablero.getCasillaAt('C',4);
		// P4
		a1 = tablero.getCasillaAt('A',1);
		e1 = tablero.getCasillaAt('E',1);
		h1 = tablero.getCasillaAt('H',1);
		h2 = tablero.getCasillaAt('H',2);
		a8 = tablero.getCasillaAt('A',8);
		e8 = tablero.getCasillaAt('E',8);
		h8 = tablero.getCasillaAt('H',8);
		c1 = tablero.getCasillaAt('C',1);
		d1 = tablero.getCasillaAt('D',1);
		d2 = tablero.getCasillaAt('D',2);
		d8 = tablero.getCasillaAt('D',8);
		f1 = tablero.getCasillaAt('F',1);
		f2 = tablero.getCasillaAt('F',2);
		f8 = tablero.getCasillaAt('F',8);
		g1 = tablero.getCasillaAt('G',1);
		g2 = tablero.getCasillaAt('G',2);
		c8 = tablero.getCasillaAt('C',8);
		g8 = tablero.getCasillaAt('G',8);

	}

	@After
	public void tearDown() {
		r0.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		rb.quitaDeCasilla();
		rn.quitaDeCasilla();
		tb.quitaDeCasilla();
		tn.quitaDeCasilla();
		pb2.quitaDeCasilla();
		pb3.quitaDeCasilla();
		tb2.quitaDeCasilla();
	}
	

	@Test
	public final void testPeonNuloNoPuedeMover() {
		assertFalse(r0.puedeMover(a2));
	}

	@Test
	public final void testPeonFueradeTableroNoPuedeMover() {
		assertFalse(rb.puedeMover(a2));
	}

	@Test
	public final void testCasillaDestinoNoValida() {
		rb.setCasilla(a2);
		assertFalse(rb.puedeMover(c0));
	}


	@Test
	public final void testCasillaDestinoNoVacia() {
		rb.setCasilla(a3);
		pb.setCasilla(a4);
		assertFalse(rb.puedeMover(a4));
		pb.quitaDeCasilla();
		pb.setCasilla(a2);
		assertFalse(rb.puedeMover(a2));
	}

	@Test
	public final void testDestinoIlegal() {
		rb.setCasilla(a2);
		assertFalse(rb.puedeMover(b5));
	}

	@Test
	public final void testPeonNoCaptura2() {
		// Mov. de captura, pero no hay pieza del contrario en destino
		rb.setCasilla(b3);
		pb.setCasilla(a4);
		assertFalse(rb.puedeMover(a4));
		pb.quitaDeCasilla();
		pb.setCasilla(c4);
		assertFalse(rb.puedeMover(c4));
	}

	// Movimientos OK
	@Test
	public final void testMovOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a2);
		assertTrue(rb.puedeMover(a3));		
		rn.setCasilla(b7);
		assertTrue(rn.puedeMover(b6));		
	}

	@Test
	public final void testMovOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a3);
		assertTrue(rb.puedeMover(b3));		
		rn.setCasilla(b5);
		assertTrue(rn.puedeMover(a5));		
	}

	@Test
	public final void testMovOk3() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a4);
		assertTrue(rb.puedeMover(b3));		
		rn.setCasilla(b5);
		assertTrue(rn.puedeMover(a4));		
	}

	@Test
	public final void testMovOk4() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(b3);
		assertTrue(rb.puedeMover(a2));		
		rn.setCasilla(a4);
		assertTrue(rn.puedeMover(b5));		
	}

	/************
	 * Capturas
	 ***********/
	
	@Test
	public final void testMovCapturaOk1() {
		// b3xa4
		rb.setCasilla(b3);
		pn.setCasilla(a4);
		assertTrue(rb.puedeMover(a4));
	}

	@Test
	public final void testMovCapturaOk2() {
		// b3xc4
		rn.setCasilla(b3);
		pb.setCasilla(c4);
		assertTrue(rn.puedeMover(c4));
	}
	
	//--------------- rey() ---------------- 
	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testMueveDestinoOcupado1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a2);
		pb.setCasilla(a3);
		rb.mueve(a3);
	}


	@Test(expected=ExcepcionMovimientoIlegal.class)
	public final void testMovimientoIlegal() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a2);
		rb.mueve(c4);
	}

	@Test
	public final void testMovReyOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		rb.setCasilla(a2);
		rb.mueve(a3);
		assertNull(a2.getPieza());
		assertSame(rb, a3.getPieza());
	}

	@Test
	public final void testMovReyCapturaOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {		
		rb.setCasilla(b3);
		pn.setCasilla(a4);
		rb.mueve(a4);
		assertNull(b3.getPieza());
		assertSame(rb, a4.getPieza());
	}

	@Test
	public final void testMovReyCapturaOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		rn.setCasilla(b3);
		pb.setCasilla(c4);
		rn.mueve(c4);
		assertNull(b3.getPieza());
		assertSame(rn, c4.getPieza());
	}

	/*****************
	 * OTROS METODOS
	 *****************/
	
	@Test
	public final void testIsMismoTipo() {
		assertTrue(rb.isMismoTipo(rn));
		assertFalse(rb.isMismoTipo(pb));
	}

	@Test
	public final void testGetTipo() {
		assertEquals('R',rb.getTipo());
	}
}
