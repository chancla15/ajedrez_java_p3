package modelo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimientoIlegal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PeonTestP3 {

	Pieza p1, pn, pn2, pn3, pb, pb2, pb3;
	Casilla c0,a2,a3,a4,a5,b3,b4,b5,b6,b7,b8,c2,c4,h2,g1,g3;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situación del juego (puedeMover() consulta el tablero, por ejemplo)
	PartidaAjedrez paj;
	Tablero tablero;

	@Before
	public void setUp() throws Exception {
		
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		p1 = new Peon(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		pn2 = new Peon(Color.NEGRO);
		pn3 = new Peon(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		pb2 = new Peon(Color.BLANCO);
		pb3 = new Peon(Color.BLANCO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		b8 = tablero.getCasillaAt('B',8);
		c4 = tablero.getCasillaAt('C',4);
		c2 = tablero.getCasillaAt('C',2);
		h2 = tablero.getCasillaAt('H',2);
		g3 = tablero.getCasillaAt('G',3);
		g1 = tablero.getCasillaAt('G',1);
		
	}

	@After
	public void tearDown() {
		p1.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		pb2.quitaDeCasilla();
		pn2.quitaDeCasilla();
		pb3.quitaDeCasilla();
		pn3.quitaDeCasilla();
	}
	
	/************************************************
	 * PEON BLANCO
	 ************************************************/
	
	@Test
	public final void testPeonNuloNoPuedeMover() {
		assertFalse(p1.puedeMover(a2));
	}

	@Test
	public final void testPeonFueradeTableroNoPuedeMover() {
		assertFalse(pn.puedeMover(a2));
	}

	@Test
	public final void testCasillaDestinoNoValida() {
		pb.setCasilla(a2);
		assertFalse(pb.puedeMover(c0));
	}


	@Test
	public final void testCasillaDestinoNoVacia() {
		pb.setCasilla(a2);
		pn.setCasilla(a3);
		assertFalse(pb.puedeMover(a3));
	}

	@Test
	public final void testCasillaDestinoNoVacia2() {
		pb.setCasilla(a2);
		pn.setCasilla(a4);
		assertFalse(pb.puedeMover(a4));
	}

	@Test
	public final void testCasillaIntermediaNoVacia() {
		pb.setCasilla(a2);
		pn.setCasilla(a3);
		assertFalse(pb.puedeMover(a4));
	}

	@Test
	public final void testPosicionNoOriginal() {
		pb.setCasilla(b3);
		assertFalse(pb.puedeMover(b5));
	}

	@Test
	public final void testDestinoIlegal() {
		pb.setCasilla(a2);
		assertFalse(pb.puedeMover(b5));
	}

	@Test
	public final void testPeonNoMueveHaciaAtras() {
		pb.setCasilla(a3);
		assertFalse(pb.puedeMover(a2));
	}
	
	@Test
	public final void testPeonNoCaptura1() {
		// Mov. de captura, pero no hay pieza en destino
		pb.setCasilla(b3);
		assertFalse(pb.puedeMover(a4));
		assertFalse(pb.puedeMover(c4));
	}

	@Test
	public final void testPeonNoCaptura2() {
		// Mov. de captura, pero no hay pieza del contrario en destino
		pb.setCasilla(b3);
		pb2.setCasilla(a4);
		assertFalse(pb.puedeMover(a4));
		pb2.quitaDeCasilla();
		pb2.setCasilla(c4);
		assertFalse(pb.puedeMover(c4));
	}

	// Movimientos OK
	@Test
	public final void testMovOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pb.setCasilla(a2);
		assertTrue(pb.puedeMover(a3));
		pb.mueve(a3);
		assertTrue(pb.puedeMover(a4));
		
	}
	
	@Test
	public final void testMovOk2() {
		pb.setCasilla(a2);
		assertTrue(pb.puedeMover(a4));
	}

	@Test
	public final void testMovCapturaOk1() {
		// b3xa4
		pb.setCasilla(b3);
		pn.setCasilla(a4);
		assertTrue(pb.puedeMover(a4));
	}

	@Test
	public final void testMovCapturaOk2() {
		// b3xc4
		pb.setCasilla(b3);
		pn.setCasilla(c4);
		assertTrue(pb.puedeMover(c4));
	}
	
	//--------------- Peon.mueve() ---------------- 
	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testMueveDestinoOcupado1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pb.setCasilla(a2);
		pn.setCasilla(a3);
		pb.mueve(a3);
	}

	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testMueveDestinoOcupado2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pb.setCasilla(a2);
		pn.setCasilla(a4);
		pb.mueve(a4);
	}

	@Test(expected=ExcepcionMovimientoIlegal.class)
	public final void testMovimientoIlegal() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pb.setCasilla(a2);
		pb.mueve(c4);
	}

	@Test
	public final void testMovPeonOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pb.setCasilla(a2);
		pb.mueve(a3);
		assertNull(a2.getPieza());
		assertSame(pb, a3.getPieza());
	}

	@Test
	public final void testMovPeonOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pb.setCasilla(a2);
		pb.mueve(a4);
		assertNull(a2.getPieza());
		assertSame(pb, a4.getPieza());
	}

	@Test
	public final void testMovPeonCapturaOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pb.setCasilla(b3);
		pn.setCasilla(a4);
		pb.mueve(a4);
		assertNull(b3.getPieza());
		assertSame(pb, a4.getPieza());
	}

	@Test
	public final void testMovPeonCapturaOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pb.setCasilla(b3);
		pn.setCasilla(c4);
		pb.mueve(c4);
		assertNull(b3.getPieza());
		assertSame(pb, c4.getPieza());
	}

	/************************************************
	 * PEON NEGRO
	 ************************************************/
	

	@Test
	public final void testNegroCasillaDestinoNoVacia() {
		pn.setCasilla(a3);
		pb.setCasilla(a2);
		assertFalse(pn.puedeMover(a2));
	}

	@Test
	public final void testNegroCasillaDestinoNoVacia2() {
		pb.setCasilla(a2);
		pn.setCasilla(a4);
		assertFalse(pn.puedeMover(a2));
	}

	@Test
	public final void testNegroCasillaIntermediaNoVacia() {
		pb.setCasilla(b6);
		pn.setCasilla(b7);
		assertFalse(pn.puedeMover(b5));
	}

	@Test
	public final void testNegroPosicionNoOriginal() {
		pn.setCasilla(b5);
		assertFalse(pn.puedeMover(b3));
	}

	@Test
	public final void testNegroDestinoIlegal() {
		pn.setCasilla(a2);
		assertFalse(pn.puedeMover(b5));
	}

	@Test
	public final void testNegroPeonNoMueveHaciaAtras() {
		pn.setCasilla(a2);
		assertFalse(pn.puedeMover(a3));
	}
	
	@Test
	public final void testNegroPeonNoCaptura1() {
		// Mov. de captura, pero no hay pieza en destino
		pn.setCasilla(b5);
		assertFalse(pn.puedeMover(a4));
		assertFalse(pn.puedeMover(c4));
	}

	@Test
	public final void testNegroPeonNoCaptura2() {
		// Mov. de captura, pero no hay pieza del contrario en destino
		pn.setCasilla(b5);
		pn2.setCasilla(a4);
		assertFalse(pn.puedeMover(a4));
		pn2.quitaDeCasilla();
		pn2.setCasilla(c4);
		assertFalse(pn.puedeMover(c4));
	}

	// Movimientos OK
	@Test
	public final void testNegroMovOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pn.setCasilla(b7);
		assertTrue(pn.puedeMover(b6));
		pn.mueve(b6);
		assertTrue(pn.puedeMover(b5));
	}
	
	@Test
	public final void testNegroMovOk2() {
		pn.setCasilla(b7);
		assertTrue(pn.puedeMover(b5));
	}

	@Test
	public final void testNegroMovCapturaOk1() {
		// b5xa4
		pn.setCasilla(b5);
		pb.setCasilla(a4);
		assertTrue(pn.puedeMover(a4));
	}

	@Test
	public final void testNegroMovCapturaOk2() {
		// b5xc4
		pb.setCasilla(c4);
		pn.setCasilla(b5);
		assertTrue(pn.puedeMover(c4));
	}
	
	//--------------- Peon.mueve() ---------------- 
	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testNegroMueveDestinoOcupado1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pb.setCasilla(a2);
		pn.setCasilla(a3);
		pn.mueve(a2);
	}

	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testNegroMueveDestinoOcupado2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pb.setCasilla(b5);
		pn.setCasilla(b7);
		pn.mueve(b5);
	}

	@Test(expected=ExcepcionMovimientoIlegal.class)
	public final void testNegroMovimientoIlegal() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		pn.setCasilla(c4);
		pn.mueve(a2);
	}

	@Test
	public final void testNegroMovPeonOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pn.setCasilla(b7);
		pn.mueve(b6);
		assertNull(b7.getPieza());
		assertSame(pn, b6.getPieza());
	}

	@Test
	public final void testNegroMovPeonOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pn.setCasilla(b7);
		pn.mueve(b5);
		assertNull(b7.getPieza());
		assertSame(pn, b5.getPieza());
	}

	@Test
	public final void testNegroMovPeonCapturaOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pb.setCasilla(b3);
		pn.setCasilla(a4);
		pn.mueve(b3);
		assertNull(a4.getPieza());
		assertSame(pn, b3.getPieza());
	}

	@Test
	public final void testNegroMovPeonCapturaOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		pb.setCasilla(b3);
		pn.setCasilla(c4);
		pn.mueve(b3);
		assertNull(c4.getPieza());
		assertSame(pn, b3.getPieza());
	}

	/*****************
	 * OTROS METODOS
	 *****************/
	
	@Test
	public final void testIsMismoTipo() {
		assertTrue(pb.isMismoTipo(pn));
		Pieza r = new Rey(Color.BLANCO);
		assertFalse(pb.isMismoTipo(r));
	}

	@Test
	public final void testGetTipo() {
		assertEquals('P',pb.getTipo());
	}

}
