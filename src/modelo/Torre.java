package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * La clase Torre
 * @author Jesus Manresa Parres
 * @version 4
 * @date 09.12.2012
 */
public class Torre extends Pieza 
{
	/** Variable privada para el tipo torre */
	private final char tipo= 'T';

	/**
	 * Constructor de Torre
	 * @param c el color
	 */
	public Torre(Color c) 
	{
		super(c);
		valor= 5;
	}

	/**
	 * Metodo que indica si dos piezas son del mismo tipo.
	 * @param p the p
	 * @return boolean
	 */
	@Override
	public boolean isMismoTipo(Pieza p)
	{
		return (tipo==p.getTipo());
	}

	/**
	 * Getter.
	 * @return char
	 */
	@Override
	public char getTipo()
	{
		return tipo;
	}

	/**
	 * Getter
	 * @return Set<Pieza>
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() 
	{
		Set<Casilla> lista= new HashSet<Casilla>();
		List<Casilla> aux= new ArrayList<Casilla>();
		Tablero t= PartidaAjedrez.getInstancia().getTablero();
		Casilla cas= getCasilla();
		
		if(cas!=null && isValida())
		{
			aux= t.getFilaIzquierda(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getFilaDerecha(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getColumnaArriba(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getColumnaAbajo(cas);
			if(aux.size()>0)
				lista.addAll(aux);
		}
		return lista;
	}
}
