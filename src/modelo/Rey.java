package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * Clase Rey.
 *
 * @author JESUS MANRESA PARRES
 * @version 3
 * @date 29.10.2012
 */
public class Rey extends Pieza 
{
	
	/** Variable privada que indica el tipo, Rey= 'R'. */
	private static final char tipo= 'R';

	/**
	 * Constructor de clase.
	 *
	 * @param c Color de la pieza
	 */
	public Rey(Color c) 
	{ 
		super(c);
		valor=0;
	}
	
	/**
	 * Metodo que indica si dos piezas son del mismo tipo.
	 *
	 * @param p the p
	 * @return boolean
	 */
	@Override
	public boolean isMismoTipo(Pieza p) { return p.getTipo()==tipo;}

	/**
	 * Getter.
	 *
	 * @return char
	 */
	@Override
	public char getTipo() { return tipo; }
	
	/**
	 * Getter
	 * @return Set<Pieza>
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas()
	{
		Set<Casilla> lista = new HashSet<Casilla>();
		List<Casilla> aux= new ArrayList<Casilla>();
		Tablero t= PartidaAjedrez.getInstancia().getTablero();
		Casilla cas= getCasilla();
			
		if(cas!=null && isValida())
		{
			aux= t.getFilaIzquierda(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
				
			aux= t.getFilaDerecha(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
				
			aux= t.getColumnaAbajo(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
				
			aux= t.getColumnaArriba(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
				
			aux= t.getDiagonalNO(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
				
			aux= t.getDiagonalNE(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
				
			aux= t.getDiagonalSO(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
				
			aux= t.getDiagonalSE(cas);
			if(aux.size()>0)
				lista.add(aux.get(0));
		}
		return lista;
	}
	
	/**
	 * Devuelve true si se puede hacer el enroque
	 * @param c la casilla donde esta la pieza
	 * @return boolean
	 */
	public boolean comprobarEnroque(Casilla c)
	{
		try
		{
			if(c==null)
				return false;
			
			Casilla cas= getCasilla();
			Casilla aux= PartidaAjedrez.getInstancia().getTablero().getCasillaAt(c.getCoordenada());
			
			if(cas!=null && !cas.isAmenazada(Pieza.changeColor(getColor())) && !haMovido())
			{
				if(aux!=null && aux.getPieza()!=null && (aux.getPieza() instanceof Torre) 
					&& !aux.getPieza().haMovido() && aux.getPieza().isMismoColor(this))
				{
					List<Casilla> lista= new ArrayList<Casilla>();
					
					if(aux.getCoordenada().getLetra()<cas.getCoordenada().getLetra())
						lista= PartidaAjedrez.getInstancia().getTablero().getFilaIzquierda(cas);
					else if(aux.getCoordenada().getLetra()>cas.getCoordenada().getLetra())
						lista= PartidaAjedrez.getInstancia().getTablero().getFilaDerecha(cas);
					
					if(lista.contains(aux))
					{
						int i=0;
						while(i<2)
						{
							if(lista.get(i).isAmenazada(Pieza.changeColor(getColor())))
								return false;
							i++;
						}
						return true;
					}
				}
			}
		}
		catch(ExcepcionCoordenadaErronea e)
		{
			System.err.println(e.getMessage());
		}
		
		return false;
	}
}
