package modelo;

import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionTurnoDelContrario;

/**
 * La interfaz EvauladorAjedrez
 * @author JESUS MANRESA PARRES
 * @version 4.0
 * @date 09/11/2012
 */
public interface EvaluadorAjedrez 
{
	/**
	 * Valor estrat�gico de las piezas del jugador del color dado en 
	 * la posici�n actual de la partida. 
	 * S�lo se tienen en cuenta las piezas que est�n sobre el tablero.
	 * @param pa la partida a evaluar
	 * @param c el color de las piezas
	 * @return double
	 */
	double evalua(PartidaAjedrez pa, Color c);
	
	/**
	 * Evalua un movimiento que le pasas por parametro
	 * @param pa la partida actual a evaluar
	 * @param c el color
	 * @param n el movimiento a ejecutar
	 * @return double
	 * @throws ExcepcionTurnoDelContrario  la excepcion
	 * @throws ExcepcionCasillaOrigenVacia  la excepcion
	 * @throws ExcepcionNoExisteMovimiento  la excepcion
	 */
	double evaluaMovimiento(PartidaAjedrez pa , Color c, int n) throws ExcepcionNoExisteMovimiento, ExcepcionCasillaOrigenVacia, ExcepcionTurnoDelContrario;
}
