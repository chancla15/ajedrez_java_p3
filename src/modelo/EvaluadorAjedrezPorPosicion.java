package modelo;

import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionTurnoDelContrario;

/**
 * La clase EvaluadorAjedrezPorValorPiezas
 * @author JESUS MANRESA PARRES
 * @version 4.0
 * @date 09/11/2012
 */
public class EvaluadorAjedrezPorPosicion implements EvaluadorAjedrez 
{

	/**
	 * Valor estrat�gico de las piezas del jugador del color dado en 
	 * la posici�n actual de la partida. 
	 * S�lo se tienen en cuenta las piezas que est�n sobre el tablero.
	 * @param pa la partida a evaluar
	 * @param c el color de las piezas
	 * @return double
	 */
	@Override
	public double evalua(PartidaAjedrez pa, Color c) 
	{
		double puntos=0;
		int dimy= pa.getTablero().getDimy();
		int i=1;
		char letra= 'A';
		Casilla cas;
		
		try
		{
			while(i<=dimy)
			{
				while(letra<='H')
				{	
					cas= pa.getTablero().getCasillaAt(letra,i);
					
					if(cas!=null && cas.getPieza()!=null && cas.getPieza().getColor().equals(c))
					{
						if(cas.getPieza() instanceof Peon)
						{
							if(((Peon) cas.getPieza()).mitadTablero())
								puntos+=1;
							if(((Peon) cas.getPieza()).mitadSuperiorTablero())
								puntos+=2;
							if(((Peon) cas.getPieza()).isDoblado())
								puntos-=1;
						}
						if(!cas.isAmenazada(Pieza.changeColor(c)))
							puntos+=1;
					}
					letra++;
				}
				letra='A';
				i++;
			}
		}
		catch(ExcepcionCoordenadaErronea e)
		{
			System.err.println(e.getMessage());
		}
		return puntos;
	}

	/**
	 * Evalua un movimiento que le pasas por parametro
	 * @param pa la partida actual a evaluar
	 * @param c el color
	 * @param n el movimiento a ejecutar
	 * @return double
	 * @throws ExcepcionTurnoDelContrario  la excepcion
	 * @throws ExcepcionCasillaOrigenVacia  la excepcion
	 * @throws ExcepcionNoExisteMovimiento  la excepcion
	 */
	@Override
	public double evaluaMovimiento(PartidaAjedrez pa, Color c, int n)
		throws ExcepcionNoExisteMovimiento, ExcepcionCasillaOrigenVacia, ExcepcionTurnoDelContrario 
	{
		if(n>=0 && n<=pa.getNumMovimientos())
			pa.ejecutaMovimiento(n);
		
		return evalua(pa,c);
	}

}
