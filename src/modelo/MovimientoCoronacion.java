package modelo;

/**
 * Clase Movimiento Coronacion
 * @author JESUS MANRESA PARRES 
 * @version 1.0
 * @date 29.10.2012
 */
public class MovimientoCoronacion extends Movimiento 
{
	/** Variable privada pieza */
	private Pieza pieza;
	
	/**
	 * Constructor de clase a partir de dos coordenadas.
	 * @param co coordenada origen
	 * @param cd coordenada destino
	 */
	public MovimientoCoronacion(Coordenada co, Coordenada cd, Pieza p) 
	{
		super(co, cd);
		pieza= p;
	}
	
	/**
	 * Getter
	 * @return Pieza
	 */
	public Pieza getPieza()
	{
		return pieza;
	}
}
