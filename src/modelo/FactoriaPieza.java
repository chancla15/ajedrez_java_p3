package modelo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import modelo.excepciones.ExcepcionPiezaDesconocida;

/**
 * Clase FactoriaPiezas
 * @author Jesus Manresa Parres
 * @vesion 4
 * @date 03.12.2012
 */
public class FactoriaPieza 
{
	/**
	 * Metodo que crea una pieza
	 * @param p el tipo de pieza a crear
	 * @param c el color
	 * @return Pieza
	 * @throws ExcepcionPiezaDesconocida una excepcion
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	public static Pieza creaPieza(String p, Color c) throws ExcepcionPiezaDesconocida
	{
		try 
		{
			Class <Pieza> cl=  (Class<Pieza>) Class.forName("modelo." + p);
			Class[] paramTypes= new Class[] {Color.class};
			Constructor <Pieza> m= cl.getConstructor(paramTypes);
			Pieza aux= (Pieza) m.newInstance(c);
			return aux;
			
		}
		catch (ClassNotFoundException e)
		{
			throw new ExcepcionPiezaDesconocida(p.charAt(0));
		} 
		catch (SecurityException e)
		{
			throw new ExcepcionPiezaDesconocida(p.charAt(0));
		} 
		catch (NoSuchMethodException e)
		{
			throw new ExcepcionPiezaDesconocida(p.charAt(0));
		}
		catch (InstantiationException e)
		{
			throw new ExcepcionPiezaDesconocida(p.charAt(0));
		} 
		catch (InvocationTargetException e)
		{
			throw new ExcepcionPiezaDesconocida(p.charAt(0));
		}
		catch (IllegalAccessException e)
		{
			throw new ExcepcionPiezaDesconocida(p.charAt(0));
		} 
		catch (IllegalArgumentException e)
		{
			throw new ExcepcionPiezaDesconocida(p.charAt(0));
		}
	}
	
	/**
	 * Metodo que crea una pieza
	 * @param p un caracter que indica una pieza
	 * @param c un color
	 * @return Pieza
	 */
	public static Pieza creaPieza(char p, Color c)
	{
			String pieza= new String("");
		
			if(p=='R')
				pieza="Rey";
			else if(p=='P')
				pieza="Peon";
			else if(p=='A')
				pieza= "Alfil";
			else if(p=='T')
				pieza= "Torre";
			else if(p=='C')
				pieza= "Caballo";
			else if(p=='D')
				pieza= "Dama";
			else 
				pieza= Character.toString(p);
			
			try
			{
				return FactoriaPieza.creaPieza(pieza, c);
			}
			catch(ExcepcionPiezaDesconocida e)
			{
				System.err.println(e.getMessage());
			}
		return null;
	}
}
