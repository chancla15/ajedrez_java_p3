package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoIlegal;


/**
 * Clase Peon.
 *
 * @author JESUS MANRESA PARRES
 * @version 3
 * @date 29.10.2012
 */
public class Peon extends Pieza
{	
	
	/** Variable privada que indica el tipo, Peon= 'P'. */
	private static final char tipo= 'P';
	
	/**
	 * Constructor de clase.
	 *
	 * @param c Color de la pieza
	 */
	public Peon(Color c) 
	{
		super(c);
		valor=1;
	}

	/**
	 * Metodo que indica si el peon esta en su fila de inicio, es dcir, aun no ah exo ningun movimiento.
	 *
	 * @return boolean
	 */
	public boolean isEnPosicionOriginal()
	{
		if(getCasilla()!=null)
		{
			if(getColor()==Color.BLANCO && getCasilla().getCoordenada().getY()==2)
				return true;
			else if(getColor()==Color.NEGRO && getCasilla().getCoordenada().getY()==7)
				return true;
		}
		return false;
	}
	
	/**
	 * Metodo que indica si una pieza puede mover (casilla dentro de tablero y no ocupada, si esta
	 * ocupada que nosea del mismo color.
	 * @param c Casilla a la que se va a mover
	 * @return boolean
	 */
	@Override
	public boolean puedeMover(Casilla c) 
	{
		try
		{
			Set<Casilla> aux= getCasillasAmenazadas();
			Pieza piezac= PartidaAjedrez.getInstancia().getPiezaAt(c.getCoordenada());
			Casilla cas= PartidaAjedrez.getInstancia().getTablero().getCasillaAt(c.getCoordenada());
		
			if(isValida() && getCasilla()!=null)
			{
				int[] dif= getDiferencia(cas);
			
				if(dif!=null)
				{	 
					if(dif[0]<0 && getColor()==Color.BLANCO)
						return false;
					else if(dif[0]>0 && getColor()==Color.NEGRO)
						return false;

					if(Math.abs(dif[0])==1 && dif[1]==0 && piezac==null)
						return true;
					else if(aux.contains(cas))
					{
						if(piezac!=null && isMismoColor(piezac) || piezac==null)
							return false;
						else
							return true;
					}
					else if(Math.abs(dif[0])==2 && dif[1]==0 && isEnPosicionOriginal() && piezac==null)
					{
						Coordenada inter= new Coordenada(cas.getCoordenada().getLetra(), cas.getCoordenada().getY() - Integer.signum(dif[0]));
						
						if(PartidaAjedrez.getInstancia().getPiezaAt(inter)==null)
							return true;
					}	
				}
			}
		}
		catch(ExcepcionCoordenadaErronea e)
		{
			System.err.println("Error: " + e.getMessage());
		}
		
		return false;
	}
	
	/**
	 * Metodo que mueve la casilla si el movimiento es valido y no esta ocupada por una pieza
	 * del mismo color. SI es de disinto color la quitara del tablero y luego asignara nuestra
	 * pieza a esa casilla.
	 * @param c una casilla
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 */
	@Override
	public void mueve(Casilla c) throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal 
	{
		try 
		{
			Movimiento m= new MovimientoOrdinario(getCasilla().getCoordenada(), c.getCoordenada());
			Pieza piezac= PartidaAjedrez.getInstancia().getPiezaAt(c.getCoordenada());
			
			if(puedeMover(c))
			{
				if(c.getPieza()!=null)
					c.quitaPieza();
			
				quitaDeCasilla();
				setCasilla(c);
				haMovido=true;
			}
			else 
			{
				if(piezac!=null)
					throw new ExcepcionCasillaDestinoOcupada(m);
				else
					throw new ExcepcionMovimientoIlegal(this,m);
			}	
		} 
		catch (ExcepcionCoordenadaErronea e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que indica si dos piezas son del mismo tipo.
	 * @param p the p
	 * @return boolean
	 */
	@Override
	public boolean isMismoTipo(Pieza p) 
	{
		return p.getTipo()==tipo;
	}

	/**
	 * Getter.
	 * @return char
	 */
	@Override
	public char getTipo() 
	{ 
		return tipo;
	}
	
	/**
	 * Devuelve true si el peon se encuentra en la ultima fila
	 * @return boolean
	 */
	public boolean isEnUltimaFila()
	{
		if(getCasilla()!=null)
		{
			if(getColor().equals(Color.BLANCO) && getCasilla().getCoordenada().getY()==8)
				return true;
			else if(getColor().equals(Color.NEGRO) && getCasilla().getCoordenada().getY()==1)
				return true;
		}
		return false;
	}
	
	/**
	 * Getter
	 * @return Set<Pieza>
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas()
	{
		Set<Casilla> lista= new HashSet<Casilla>();
		List<Casilla> aux= new ArrayList<Casilla>();
		Tablero t= PartidaAjedrez.getInstancia().getTablero();
		Casilla cas= getCasilla();
		
		if(cas!=null && isValida())
		{
			if(getColor().equals(Color.BLANCO))
			{
				aux= t.getDiagonalNO(cas);
				if(aux.size()>0)
					lista.add(aux.get(0));
					
				aux= t.getDiagonalNE(cas);
				if(aux.size()>0)
					lista.add(aux.get(0));
			}
			else
			{
				aux= t.getDiagonalSO(cas);
				if(aux.size()>0)
					lista.add(aux.get(0));
					
				aux= t.getDiagonalSE(cas);
				if(aux.size()>0)
					lista.add(aux.get(0));
			}
		}
		return lista;
	}
	
	/**
	 * Comrpueba el peon esta en la siguiente fila a la mitad del tablero
	 * @return boolean
	 */
	public boolean mitadTablero()
	{
		if(getCasilla()!=null)
		{
			if(getColor().equals(Color.NEGRO) && getCasilla().getCoordenada().getY()==4)
				return true;
			if(getColor().equals(Color.BLANCO) && getCasilla().getCoordenada().getY()==5)
				return true;
		}
		return false;
	}
	
	/**
	 * Comprueba si el peon esta en la fila siguiente a la siguiente de 
	 * la mitad del tablero (Blancas: 6,7,8) (Negras: 3,2,1)
	 * @return boolean
	 */
	public boolean mitadSuperiorTablero()
	{
		if(getCasilla()!=null)
		{
			if(getColor().equals(Color.NEGRO) && getCasilla().getCoordenada().getY()<4)
				return true;
			if(getColor().equals(Color.BLANCO) && getCasilla().getCoordenada().getY()>5)
				return true;
		}
		return false;
	}
	
	/**
	 * Metodo que indica si un peon esta desdoblado
	 * @return boolean
	 */
	public boolean isDoblado()
	{
		if(getCasilla()!=null)
		{
			Tablero t= PartidaAjedrez.getInstancia().getTablero();
			List<Casilla> lista= t.getColumnaArriba(getCasilla());
			
			if(lista!=null)
			{
				Pieza p;
				int i=0;
				while(i<lista.size())
				{
					p= lista.get(i).getPieza();
					
					if(p!=null && (p instanceof Peon) && isMismoColor(p))
						return true;
					i++;
				}
			}
			
			lista= t.getColumnaAbajo(getCasilla());
			if(lista!=null)
			{
				Pieza p;
				int i=0;
				while(i<lista.size())
				{
					p= lista.get(i).getPieza();
					
					if(p!=null && (p instanceof Peon) && isMismoColor(p))
						return true;
					i++;
				}
			}
		}
		
		return false;
	}
}
