package modelo;

/**
 * Clase abstracta Movimiento.
 * @author JESUS MANRESA PARRES
 * @version 4.0
 * @date 29.10.2012
 */
public abstract class Movimiento
{
	
	/** Vector de coordenadas con origen y destino */
	private Coordenada coordenadas[];
	
	/**
	 * Constructor de clase a partir de dos coordenadas.
	 * @param co coordenada origen
	 * @param cd coordenada destino
	 */
	public Movimiento(Coordenada co, Coordenada cd)
	{
		coordenadas= new Coordenada[2];
		coordenadas[0]= new Coordenada(co);
		coordenadas[1]= new Coordenada(cd);
	}
	
	/**
	 * Getter.
	 * @return Coordenada
	 */
	public Coordenada getCoordenadaOrigen() { return coordenadas[0]; }
	
	/**
	 * Getter.
	 * @return Coordenada
	 */
	public Coordenada getCoordenadaDestino() { return coordenadas[1]; }	
}


