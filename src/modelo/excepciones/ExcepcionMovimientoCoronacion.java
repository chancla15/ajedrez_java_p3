package modelo.excepciones;

import modelo.Pieza;

/**
 * Clase ExcepcionMovimientoCoronacion
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 03.12.2012
 */
@SuppressWarnings("serial")
public class ExcepcionMovimientoCoronacion extends ExcepcionMovimiento
{
	/** Variable privada peon */
	private Pieza peon;

	/** Variable privada pieza */
	private Pieza pieza;

	/**
	 * Constructor de clase
	 * @param peon un peon
	 * @param pieza una pieza
	 */
	public ExcepcionMovimientoCoronacion(Pieza peon, Pieza pieza)
	{
		super(null);
		this.peon= peon;
		this.pieza= pieza;
	}
	
	/**
	 * Getter
	 * @return Pieza
	 */
	public Pieza getPieza()
	{
		return pieza;
	}
	
	/**
	 * Getter
	 * @return Pieza
	 */
	public Pieza getPeon()
	{
		return peon;
	}
	
	/**
	 * Getter
	 * @return String
	 */
	public String getMessage()
	{
		return new String("Error Movimiento Coronacion: " + peon + " y " + pieza);
	}
}
