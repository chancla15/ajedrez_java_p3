package modelo.excepciones;

import modelo.Movimiento;
import modelo.Pieza;

/**
 * Clase ExcepcionMovimientoIlegal.
 *
 * @author JESUS MANRESA PARRES 
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public class ExcepcionMovimientoIlegal extends ExcepcionMovimiento 
{
	
	/** Variable privada de clase que representa una pieza. */
	private Pieza p;
	
	/**
	 * Constructor de clase.
	 *
	 * @param p una pieza
	 * @param m un movimiento
	 */
	public ExcepcionMovimientoIlegal(Pieza p, Movimiento m)  { super(m); this.p=p; }
	
	
	/**
	 * Getter
	 * @return Pieza
	 */
	public Pieza getPieza()
	{
		return p;
	}
	
	/**
	 * Metodo que devuelve una cadena.
	 *
	 * @return String
	 */
	@Override
	public String getMessage()  { return new String("Error Movimiento Ilegal: " + p); }
}
