package modelo.excepciones;


/**
 * Clase ExcepcionNoExisteMovimiento.
 *
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public class ExcepcionNoExisteMovimiento extends ExcepcionMovimiento 
{
	
	/** Variable privada de clase. */
	private int indice;
	
	/**
	 * Constructor de clase.
	 *
	 * @param indice un entero
	 */
	public ExcepcionNoExisteMovimiento(int indice) 
	{ 
		super(null); 
		this.indice=indice; 
	}
	
	/**
	 * Metodo que devuelve una cadena con el error.
	 *
	 * @return String
	 */
	@Override
	public String getMessage()  { return new String("Error No Existe Movimiento: " + indice); }
}
