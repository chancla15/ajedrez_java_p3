package modelo.excepciones;

/**
 * Clase ExcepcionCoordenadaErronea.
 *
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public class ExcepcionCoordenadaErronea extends Exception 
{
	
	/** Variable privada de clase que indica la letra. */
	private char letra;
	
	/** Variable privada de clase que indica la y. */
	private int y;
	
	/**
	 * Constructor de clase.
	 *
	 * @param letra la letra
	 * @param y la y
	 */
	public ExcepcionCoordenadaErronea(char letra, int y) {
		super();
		this.letra = letra;
		this.y = y;
	}

	/**
	 * Getter.
	 *
	 * @return char
	 */
	public char getLetra() { return letra; }

	/**
	 * Getter.
	 *
	 * @return int
	 */
	public int getY() { return y; }
	
	/**
	 * Metodo que devuelve una cadena con el erro.
	 *
	 * @return String
	 */
	public String getMessage() { return ("Error Coordenada erronea: "+letra+y); }
}
