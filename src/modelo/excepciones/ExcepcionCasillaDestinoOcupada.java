package modelo.excepciones;

import modelo.Movimiento;


/**
 * Clase ExcepcionCasillaDestinoOcupada.
 *
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public class ExcepcionCasillaDestinoOcupada extends ExcepcionMovimiento
{
	
	/**
	 * Constructor de clase.
	 *
	 * @param m un movimiento
	 */
	public ExcepcionCasillaDestinoOcupada(Movimiento m) { super(m); }
	
	/**
	 * Metodo que devuelve un mensaje.
	 *
	 * @return String
	 */
	@Override
	public String getMessage() { return new String("Error Casilla Destino Ocupada"); }
}
