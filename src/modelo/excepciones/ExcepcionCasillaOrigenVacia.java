package modelo.excepciones;

import modelo.Movimiento;

/**
 * Clase ExcepcionCasillaOrigenVacia.
 *
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public class ExcepcionCasillaOrigenVacia extends ExcepcionMovimiento 
{
	
	/**
	 * Constructor de clase.
	 *
	 * @param m un movimiento
	 */
	public ExcepcionCasillaOrigenVacia(Movimiento m) { super(m); }
	
	/**
	 * Metodo que devuelve una cadena con el error.
	 *
	 * @return String
	 */
	@Override
	public String getMessage()  { return new String("Error Casilla Origen Vacia"); }
}
