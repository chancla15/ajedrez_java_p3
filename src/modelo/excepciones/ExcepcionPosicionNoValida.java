package modelo.excepciones;
import modelo.Coordenada;

/**
 * Clase ExcepcionPosicionNoValida.
 *
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public class ExcepcionPosicionNoValida extends Exception 
{
	
	/** Variable privada de tipo coordenada. */
	private Coordenada coordenada;
	
	/**
	 * Constructor de clase.
	 *
	 * @param c una coordenada
	 */
	public ExcepcionPosicionNoValida(Coordenada c)
	{
		super();
		coordenada=c;
	}
	
	/**
	 * Getter.
	 *
	 * @return coordenada
	 */
	public Coordenada getCoordenada() { return coordenada; }
	
	/**
	 * Getter.
	 *
	 * @return String
	 */
	public String getMessage() { return new String("Error Posicion no valida: " + coordenada); }
}
