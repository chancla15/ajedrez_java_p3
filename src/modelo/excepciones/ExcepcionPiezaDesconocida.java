package modelo.excepciones;

/**
 * Clase ExcepcionPiezaDesconocida.
 *
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public class ExcepcionPiezaDesconocida extends Exception 
{
	
	/** Variable privada de clase que indica la letra. */
	private char letra;
	
	/**
	 * Constructor de clase.
	 *
	 * @param letra la letra de tipo char
	 */
	public ExcepcionPiezaDesconocida(char letra)
	{
		super();
		this.letra= letra;
	}
	
	/**
	 * Getter.
	 *
	 * @return char
	 */
	public char getLetra() { return letra; }
	
	/**
	 * Getter.
	 *
	 * @return String
	 */
	public String getMessage() { return new String("Error Pieza Desconocida: " + letra);}
	
}
