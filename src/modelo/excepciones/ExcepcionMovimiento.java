package modelo.excepciones;

import modelo.Movimiento;

/**
 * Clase ExcepcionMovimiento.
 *
 * @author JESUS MANRESA PARRES 
 * @version 1.0
 * @date 05.11.2012
 */
@SuppressWarnings("serial")
public abstract class ExcepcionMovimiento extends Exception 
{
	
	/** Variable de clase de tipo movimiento. */
	private Movimiento mov;
	
	/**
	 * Constructor de clase.
	 *
	 * @param mov un movimiento
	 */
	public ExcepcionMovimiento(Movimiento mov) { super(); this.mov=mov;}
	
	/**
	 * Getter.
	 *
	 * @return Movimiento
	 */
	public Movimiento getMovimiento() { return mov; }
	
	/**
	 * Metodo abstracto que devolvera una cadena con el error.
	 *
	 * @return String
	 */
	public abstract String getMessage();
}
