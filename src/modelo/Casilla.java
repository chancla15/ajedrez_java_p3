package modelo;

import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * La clase Casilla.
 * @author JESUS MANRESA PARRES
 * @version 4
 * @date 04.10.2012
 */
public class Casilla 
{
	
	/** Variable privada de clase que representa la coordenada. */
	private Coordenada coordenada;
	
	/** Variable privada de clase que representa el color. */
	private Color color;
	
	/** Variable privada de clase que representa la pieza. */
	private Pieza pieza;
	
	/** Variable que comprobara si el metodo setPieza es llamado o no desde setCasilla. */
	private boolean mutex;
	
	/** Constante estatica de clase que representa una casilla nula. */
	public static final Casilla CasillaError= new Casilla();
	
	/**
	 * Constructor de Casilla por parametros.
	 *
	 * @param col indica el color al que pondremos la casilla
	 * @param cor indica la coordeanda a la que pondremos la casilla
	 */
	public Casilla(Color col, Coordenada cor)
	{
		color= col;
		coordenada= new Coordenada(cor);
		pieza= null;
		mutex=false;
	}
	
	/**
	 * Constructor de Casilla por defecto.
	 */
	public Casilla()
	{
		coordenada= new Coordenada();
		color= Color.NULO;
		pieza= null;
		mutex=false;
	}
	
	/**
	 * Metodo que nos indica si la casilla esta ocupada o no.
	 *
	 * @return boolean
	 */
	public boolean isOcupada()
	{		
		if(pieza!=null) return pieza.isValida(); 
		return false;
	}
	
	/**
	 * Metodo que devuelve la pieza, en caso de no tener devuelve null.
	 *
	 * @return Pieza la pieza de la casilla
	 */
	public Pieza getPieza() { return pieza; }
	
	/**
	 * Metodo que nos indica si una casilla es nula (no tiene color).
	 *
	 * @return boolean true/false
	 */
	public boolean isNula() { return color==Color.NULO; }
	
	/**
	 * Metodo que asigna una pieza a una casilla.
	 *
	 * @param p la pieza a asignar
	 * @return boolean true/false
	 */
	public boolean setPieza(Pieza p)
	{
		/* Si se cumple la relacion */
		if(pieza==p && this==p.getCasilla())
			return true;
		else
		{
			//Si la casilla tiene pieza asignada y no ha sido llamada desde setCasilla
			if(isOcupada() && !mutex)
				return false;
			//Si la pieza que le pasas por parametro tiene casilla asignada y no ha sido enviada por setCasilla
			if(p.getCasilla()!=null && !mutex)
				return false;
		
			if(p.isValida() && !isNula())
			{
				pieza=p;
		
				if(!mutex)
				{
					pieza.changeMutex();
					pieza.setCasilla(this);
					pieza.changeMutex();
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Metodo que quita la pieza de la casilla y la devuelve.
	 * @return Pieza devuelve la pieza que tenia asignada
	 */
	public Pieza quitaPieza()
	{
		if(pieza!=null)
		{ 
			Pieza aux= pieza;
			
			if(!mutex)
			{
				pieza.changeMutex();
				pieza.quitaDeCasilla();
				pieza.changeMutex();
			}
			pieza=null;
			return aux;
		}
		return null;
	}
	
	/**
	 * Metodo que devuelve el color de la casilla.
	 * @return Color el color de la casilla
	 */
	public Color getColor() { return color; }
	
	/**
	 * Metodo que asigna un color a una casilla.
	 * @param c el color a asignar
	 */
	public void setColor(Color c) 
	{
		if(c!=Color.NULO)
			color= c; 
	}
	
	/**
	 * Metodo que devuelve la coordenada de la casilla.
	 * @return Coordenada
	 */
	public Coordenada getCoordenada() { return coordenada; }
	
	/**
	 * Metodo que asigna una coordenada a la casilla
	 * @param c columna de la casilla
	 * @param y fila de la casilla
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public void setCoordenada(char c, int y) throws ExcepcionCoordenadaErronea
	{
			coordenada=null;
			coordenada= new Coordenada(c,y);
	}
	
	/**
	 * Metodo equals para comparar si dos objetos son iguales.
	 * @param object es un objecto de la superclase Object
	 * @return boolean un booleano dependiendo de si la comparacion es correcta
	 */
	public boolean equals(Object cas)
	{
		if(cas instanceof Casilla)
		{
			if(((Casilla) cas).getColor().equals(color) && ((Casilla) cas).getCoordenada().equals(coordenada))
					return true;
		}
		return false;
	}
	
	/**
	 * Cambia mutex de estado.
	 */
	public void changeMutex() { mutex=!mutex; }
	
	/**
	 * Devuelve el estado de mutex.
	 * @return boolean
	 */
	public boolean getMutex() { return mutex; }
	
	/**
	 * Devuelve TRUE si la casilla esta amenazada por alguna pieza del color dado
	 * @param c el color dado
	 * @return boolean
	 */
	public boolean isAmenazada(Color c)
	{
		return (PartidaAjedrez.getInstancia().getTablero().getAmenazas(this, c).size()>0);
	}
}
