package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Clase Dama
 * @author JESUS MANRESA PARRES
 * @version 3
 * @date 29.10.2012
 */
public class Dama extends Pieza 
{

	/** Variable privada que indica el tipo, Dama= 'D'. */
	private static final char tipo= 'D';
	
	/**
	 * Constructor de dama
	 * @param c el color de la dama
	 */
	public Dama(Color c) 
	{
		super(c);
		valor= 9;
	}

	/**
	 * Devuelve si es el mismo tipo
	 * @param p la pieza
	 * @return boolean
	 */
	@Override
	public boolean isMismoTipo(Pieza p) 
	{
		return (p.getTipo()==tipo);
	}

	/**
	 * Getter.
	 * @return char
	 */
	@Override
	public char getTipo() 
	{
		return tipo;
	}

	/**
	 * Getter
	 * @return Set<Pieza>
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas()
	{
		Set<Casilla> lista= new HashSet<Casilla>();
		List<Casilla> aux= new ArrayList<Casilla>();
		Tablero t= PartidaAjedrez.getInstancia().getTablero();
		Casilla cas= getCasilla();
	
		if(cas!=null && isValida())
		{
			aux= t.getFilaIzquierda(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getFilaDerecha(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getColumnaArriba(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getColumnaAbajo(cas);
			if(aux.size()>0)
				lista.addAll(aux);
			
			aux= t.getDiagonalNE(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getDiagonalNO(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getDiagonalSE(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getDiagonalSO(cas);
			if(aux.size()>0)
				lista.addAll(aux);
		}
		return lista;
	}
}
