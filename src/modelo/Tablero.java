package modelo;

import java.util.ArrayList;
import java.util.List;
import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * La Clase Tablero.
 *
 * @author JESUS MANRESA PARRES
 * @version 3.0
 * @date 04.10.2012
 */
public class Tablero 
{
	
	/** Variable privada de clase que indica el numero de filas, default 8. */
	private int dimy= 8;
	
	/** Variable privada de clase que indica el numero de columnas, default 8. */
	private int dimx= 8;
	
	/** Variable privada de clase que indica el numero maximo de columnas. */
	public final int MAX_DIMX= 26;
	
	/** Variable privada de clase que indica el numero maximo de filas. */
	public final int MAX_DIMY= Integer.MAX_VALUE;
	
	/** Variable privada de clase donde se almacenaran las casillas con sus coordenadas y colores, simula el tablero. */
	private Casilla table[][];
	
	
	/**
	 * Constructor por parametros de tablero.
	 *
	 * @param dimx el numero de columnas
	 * @param dimy el numero de filas
	 */
	public Tablero(int dimx, int dimy)
	{
		if(dimy%2==0 && dimy>0 && dimy<=MAX_DIMY) this.dimy= dimy;
		if(dimx%2==0 && dimx>0 && dimx<=MAX_DIMX) this.dimx= dimx;
		
		table= new Casilla[this.dimy][this.dimx];
		
		Color coloraux= Color.NULO;
		char letra='A';
		int i= this.dimy-1;
		int j= this.dimx-1;
		int fila=this.dimy;
		
		try
		{
			while(i>=0)
			{
				while(j>=0)
				{
					if((i+j)%2==0) coloraux= Color.BLANCO;
					else coloraux= Color.NEGRO;
					table[i][j]= new Casilla(coloraux, new Coordenada(letra, fila));
					letra++;
					j--;
				}
				i--;
				fila--;
				letra='A';
				j= this.dimx-1;
			}
		}
		catch(ExcepcionCoordenadaErronea e)
		{
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Constructor de copia de tablero.
	 * @param t un objeto de tipo tablero
	 */
	public Tablero(Tablero t)
	{
		dimx= t.getDimx();
		dimy= t.getDimy();
		
		table= new Casilla[dimy][dimx];
		char letra='A';
		
		int i=dimy-1;
		int j=dimx-1;
		int aux=j;
		int fila= dimy;
		
		try
		{
			while(i>=0)
			{
				letra='A';
				j=aux;
				while(j>=0)
				{
					table[i][j]= new Casilla(t.getCasillaAt(letra,fila).getColor(), t.getCasillaAt(letra,fila).getCoordenada());
					letra++;
					j--;
				}
				fila--;
				i--;
			}
		}
		catch(ExcepcionCoordenadaErronea e)
		{
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Getter.
	 * @return dimx numero de columnas
	 */
	public int getDimx() { return dimx; }
	
	/**
	 * Getter.
	 * @return dimy el numero de filas
	 */
	public int getDimy() { return dimy; }
	
	/**
	 * Devuelve la casilla que contiene la coordenada c, en caso de no existir devolvera CasillaError.
	 *
	 * @param c un objeto de tipo Coordenada
	 * @return Casilla una casilla
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public Casilla getCasillaAt(Coordenada c) throws ExcepcionCoordenadaErronea
	{
		int i=dimy-1;
		int j=dimx-1;
		
		while(i>=0)
		{
			while(j>=0)
			{
				if(table[i][j].getCoordenada().equals(c))
					return table[i][j];
				j--;
			}
			i--;
			j=dimx-1;
		}
		throw new ExcepcionCoordenadaErronea(c.getLetra(), c.getY());
	}
	
	/**
	 * Devuelve una casilla con la coordenada (c,y).
	 *
	 * @param c la letra de la coordenada
	 * @param y la fila de la coordenada
	 * @return Casilla
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public Casilla getCasillaAt(char c, int y) throws ExcepcionCoordenadaErronea 
	{ 
		return getCasillaAt(new Coordenada(c,y)); 
	}
	
	/**
	 * Coloca una pieza en el tablero.
	 *
	 * @param c la coordenada a la que vamos a insertar la pieza
	 * @param p la pieza que vamos a insertar
	 * @return true/false
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public boolean colocaPiezaAt(Coordenada c, Pieza p) throws ExcepcionCoordenadaErronea 
	{ 
		return getCasillaAt(c).setPieza(p); 
	}
	
	/**
	 * Convierte el tablero a cadena.
	 * @return String
	 */
	public String toString()
	{
		String cadena= new String("");
		int i= dimy-1;
		int j= dimx-1;
		
		while(i>=0)
		{
			while(j>=0)
			{
				if(table[i][j].isOcupada()) 
					cadena+="1";
				else
					cadena+="0";
				j--;
			}
			cadena+="\n";
			i--;
			j= dimx-1;
		}
		return cadena;
	}
	
	/**
	 * Devuelve una lista con las casillas en la fila izquierda
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getFilaIzquierda(Casilla c)
	{
		if(c!=null)
			return getLista(c, -1, 0);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las casillas en la fila derecha
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getFilaDerecha(Casilla c)
	{
		if(c!=null)
			return getLista(c, 1, 0);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las casillas en la columna Arriba
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getColumnaArriba(Casilla c)
	{
		if(c!=null)
			return getLista(c, 0, 1);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las casillas en la columna abajo
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getColumnaAbajo(Casilla c)
	{
		if(c!=null)
			return getLista(c, 0, -1);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las casillas en la diagonal NorOEste
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getDiagonalNO(Casilla c)
	{
		if(c!=null)
			return getLista(c, -1, 1);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las casillas en la diagonal NorEste
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getDiagonalNE(Casilla c)
	{
		if(c!=null)
			return getLista(c, 1, 1);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las casillas en la diagonal SurOEste
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getDiagonalSO(Casilla c)
	{
		if(c!=null)
			return getLista(c, -1, -1);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las casillas en la diagonal SurEste
	 * @param c la casilla central
	 * @return List<Casilla>
	 */
	public List<Casilla> getDiagonalSE(Casilla c)
	{
		if(c!=null)
			return getLista(c, 1, -1);
		else
			return new ArrayList<Casilla>();
	}
	
	/**
	 * Devuelve una lista con las posiciones a las que puede ir el caballo
	 * @param c la Casilla c
	 * @return List<Casilla>
	 */
	public List<Casilla> getSaltosCaballo(Casilla c)
	{
		if(c==null)
			return new ArrayList<Casilla>();
			
		List<Casilla> lista= new ArrayList<Casilla>();
		Casilla aux;
		Coordenada caux;
		
		aux= new Casilla();
		caux= new Coordenada(c.getCoordenada());
		
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()+1), caux.getY()+2);
				if(aux!=null)
					lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
			
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()+2), caux.getY()+1);
					if(aux!=null)
						lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
			
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()+2), caux.getY()-1);
				if(aux!=null)
					lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
			
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()+1), caux.getY()-2);
				if(aux!=null)
					lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
			
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()-1), caux.getY()-2);
				if(aux!=null)
					lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
			
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()-2), caux.getY()-1);
				if(aux!=null)
					lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
			
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()-2), caux.getY()+1);
				if(aux!=null)
					lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
			
			try
			{
				aux=getCasillaAt((char)(caux.getLetra()-1), caux.getY()+2);
				if(aux!=null)
					lista.add(aux);
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			} 
		return lista;
	}
	
	/**
	 * Devuelve una lista dependiendo de los valores difH y difV
	 * @param c la casilla central
	 * @param difH valor horizontal
	 * @param difV valor vertical
	 * @return List<Casilla>
	 */
	public List<Casilla> getLista(Casilla c, int difH, int difV)
	{
		List<Casilla> lista= new ArrayList<Casilla>();
		char l= c.getCoordenada().getLetra();
		int y= c.getCoordenada().getY();
		
		try
		{
			Casilla aux= new Casilla();
			
			while(aux!=null)
			{
				aux= getCasillaAt((char)(l+difH), y+difV);
				if(aux!=null)
					lista.add(aux);
				
				if(aux.isOcupada())
					aux=null;
				
				l+=difH;
				y+=difV;
			}
		}
		catch(ExcepcionCoordenadaErronea e)
		{
			System.err.println(e.getMessage());
		}	
		return lista;
	}
	
	/**
	 * Devuelve la lista de piezas del color establecido que amenazan a una casilla
	 * @param cas la casilla amenazada
	 * @param col el color
	 * @return List<Pieza>
	 */
	public List<Pieza> getAmenazas(Casilla cas, Color col)
	{
		List<Pieza> lista= new ArrayList<Pieza>();
		Casilla c= new Casilla();
		
		if(cas!=null && col!=null && col!=Color.NULO)
		{
			try
			{
				int i=1;
				char letra='A';
				Casilla aux= getCasillaAt(cas.getCoordenada());
				
				while(i<=dimy)
				{
					while(letra<='H')
					{
						c= getCasillaAt(letra, i);
						
						if(c!=null && c.getPieza()!=null && c.getPieza().getCasillasAmenazadas().contains(aux) && c.getPieza().getColor().equals(col))
							lista.add(c.getPieza());
						letra++;
					}
					i++;
					letra='A';
				}
			}
			catch(ExcepcionCoordenadaErronea e)
			{
				System.err.println(e.getMessage());
			}
		}
		
		return lista;
	}
}