package modelo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Clase Caballo
 * @author JESUS MANRESA PARRES
 * @version 3
 * @date 29.10.2012
 */
public class Caballo extends Pieza 
{
	/** Variable privada que indica el tipo, Caballo= 'C'. */
	private static final char tipo= 'C';
	
	/**
	 * Constructor de clase caballo
	 * @param c el color
	 */
	public Caballo(Color c)
	{
		super(c);
		valor= 3;
	}

	/**
	 * Devuelve si es el mismo tipo
	 * @param p la pieza
	 * @return boolean
	 */
	@Override
	public boolean isMismoTipo(Pieza p)
	{
		return (tipo==p.getTipo());
	}

	/**
	 * Getter.
	 * @return char
	 */
	@Override
	public char getTipo()
	{
		return tipo;
	}

	/**
	 * Getter
	 * @return Set<Pieza>
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas()
	{
		Set<Casilla> lista= new HashSet<Casilla>();
		Casilla cas= getCasilla();
				
		if(cas!=null && isValida())
		{
			List<Casilla> aux= PartidaAjedrez.getInstancia().getTablero().getSaltosCaballo(cas);
			
			if(aux!=null)
				lista.addAll(aux);
		}
		
		return lista;
	}
}
