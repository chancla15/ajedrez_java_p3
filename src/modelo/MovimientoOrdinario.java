package modelo;

/**
 * Clase Movimiento Ordinario
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 29.10.2012
 */
public class MovimientoOrdinario extends Movimiento
{
	
	/**
	 * Constructor de clase a partir de dos coordenadas.
	 * @param co coordenada origen
	 * @param cd coordenada destino
	 */
	public MovimientoOrdinario(Coordenada co, Coordenada cd) 
	{
		super(co, cd);
	}

}
