package modelo;

/**
 * Enum Color
 * @author JESUS MANRESA PARRES
 * @version 1.0
 * @date 04.10.2012
 */
public enum Color 
{
	
	/** Color Nulo */
	NULO,
	
	/** Color Negro */
	NEGRO,
	
	/** Color Blanco */
	BLANCO
}
