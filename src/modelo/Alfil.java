package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Clase Alfil.
 * @author JESUS MANRESA PARRES 
 * @version 3
 * @date 29.10.2012
 */
public class Alfil extends Pieza
{
	/** Variable privada que indica el tipo, Alfil= 'A'. */
	private static final char tipo= 'A';

	/**
	 * Constructor de alfil
	 * @param c el color
	 */
	public Alfil(Color c) 
	{
		super(c);
		valor= 3;
	}

	/**
	 * Devuelve si es el mismo tipo
	 * @param p una pieza
	 * @return boolean
	 */
	@Override
	public boolean isMismoTipo(Pieza p) 
	{
		return (tipo==p.getTipo());
	}

	/**
	 * Getter.
	 * @return char
	 */
	@Override
	public char getTipo() 
	{
		return tipo;
	}

	/**
	 * Getter
	 * @return Set<Pieza>
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() 
	{
		Set<Casilla> lista= new HashSet<Casilla>();
		List<Casilla> aux= new ArrayList<Casilla>();
		Tablero t= PartidaAjedrez.getInstancia().getTablero();
		Casilla cas= getCasilla();
				
		if(cas!=null && isValida())
		{
			aux= t.getDiagonalNE(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getDiagonalNO(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getDiagonalSE(cas);
			if(aux.size()>0)
				lista.addAll(aux);
				
			aux= t.getDiagonalSO(cas);
			if(aux.size()>0)
				lista.addAll(aux);
		}
		return lista;
	}

}
