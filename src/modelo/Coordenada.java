 package modelo;

import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * Clase Coordenada.
 *
 * @author Jesus Manresa Parres 
 * @vesion 1.4
 * @date 13.09.2012
 */
public class Coordenada 
{
	
	/** Variable de clase que representa una coordenada erronea. */
	public static final Coordenada coordenadaError= new Coordenada();
	
	/** Atributo privado de clase que representa la columna. */
	private char letra;
	
	/** Atributo privado de clase que representa la fila. */
	private int y;
	
	/**
	 * Constructor por defecto de clase Coordenada.
	 */
	public Coordenada()
	{
		letra='0';
		y=0;
	}
	
	/**
	 * Constructor por parametros de clase Coordenada.
	 *
	 * @param le representa la columna
	 * @param y representa la fila
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public Coordenada(char le, int y) throws ExcepcionCoordenadaErronea
	{
		if(y<0 || le<'A' || le>'Z') throw new ExcepcionCoordenadaErronea(le,y);
		else
		{
			letra=le;
			this.y=y;
		}
	}
	
	/**
	 * Constructor de copia de clase Coordenada.
	 *
	 * @param otra es un objeto de tipo Coordenada
	 */
	public Coordenada(Coordenada otra)
	{
		letra= otra.getLetra();
		y= otra.getY();
	}
	
	/**
	 * Getter.
	 *
	 * @return letra devuelve la columna de la coordenada
	 */
	public char getLetra() { return letra;}
	
	/**
	 * Getter.
	 *
	 * @return y devuelve la fila de la coordenada
	 */
	public int getY() { return y;}
	
	/**
	 * Metodo para mostrar la coordenada por pantalla.
	 */
	public void imprimir() { System.out.print(toString()); }
	
	/**
	 * Metodo equals para comparar si dos objetos son iguales.
	 *
	 * @param object es un objecto de la superclase Object
	 * @return boolean un booleano dependiendo de si la comparacion es correcta
	 */
	public boolean equals(Object object)
	{
		if(object instanceof Coordenada)
		{
			if(((Coordenada) object).getY()==y && ((Coordenada) object).getLetra()==letra) return true;
		}
		return false;
	}
	
	/**
	 * Metodo que convierte a string un objeto Coordenada.
	 *
	 * @return sb.toString() devuelve un cadena con los datos de la coordenada
	 */
	public String toString()
	{
		StringBuilder sb= new StringBuilder();
		sb.append(letra).append(y);
		return sb.toString();
	}
}
