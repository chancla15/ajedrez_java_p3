package mains;

import java.io.FileNotFoundException;

import modelo.*;

/**
@author JESUS MANRESA PARRES
@version 13.09.2012
 **/
public class Main {

	/**
	 * Clase principal de la P1
	 * @param args los params
	 * @throws ExcepcionCasillaDestinoOcupada  la excepcion
	 * @throws ExcepcionMovimientoIlegal  la excepcion
	 * @throws FileNotFoundException  la excepcion
	 */
	public static void main(String args[]) throws FileNotFoundException 
	{ 
		  PartidaAjedrez pa=PartidaAjedrez.getInstancia(); // obtenemos el objeto que representa a la aplicación
		  pa.setEntrada(args[0]); // args[0] es el fichero de entrada, args[1] el de salida
		  pa.setSalida(args[1]);
		  pa.run(); // control principal de la aplicación
		  System.out.println(pa.toString());
	}
}
